import { schema } from 'normalizr';

export const user = new schema.Entity('users');
export const usersArray = new schema.Array(user);

export const course = new schema.Entity('courses', {
    managers: [user]
});
export const coursesArray = new schema.Array(course);

export const language = new schema.Entity('languages');
export const languagesArray = new schema.Array(language);
