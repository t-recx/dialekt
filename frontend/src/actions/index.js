import { normalize } from 'normalizr'
import * as schema from './schema'
import * as auth from '../api/auth'
import * as courses from '../api/courses'
import * as languages from '../api/languages'
import { 
    SIGNUP_FAILURE, SIGNUP_REQUEST, SIGNUP_SUCCESS,
    LOGOUT_FAILURE, LOGOUT_REQUEST, LOGOUT_SUCCESS,
    STATUS_FAILURE, STATUS_REQUEST, STATUS_ONLINE, STATUS_OFFLINE,
    SAVE_COURSE_REQUEST, SAVE_COURSE_FAILURE, SAVE_COURSE_SUCCESS,
    FETCH_COURSES_FAILURE, FETCH_COURSES_REQUEST, FETCH_COURSES_SUCCESS,
    FETCH_COURSE_FAILURE, FETCH_COURSE_REQUEST, FETCH_COURSE_SUCCESS,
    FETCH_LANGUAGES_FAILURE, FETCH_LANGUAGES_REQUEST, FETCH_LANGUAGES_SUCCESS,
    LOGIN_FAILURE, LOGIN_REQUEST, LOGIN_SUCCESS
} from './actionTypes';

export function getSignUpRequest (username, email, password) {
    return {
        type: SIGNUP_REQUEST,
        username,
        email,
        password
    }
}

export function getLoginRequest (username, password) {
    return {
        type: LOGIN_REQUEST,
        username,
        password
    }
}

export function getSaveCourseRequest (id, name, description, language_id) {
    return {
        type: SAVE_COURSE_REQUEST,
        id,
        name,
        description,
        language_id,
    }
}

export const saveCourse = (id, name, description, language_id) => (dispatch) => {
    dispatch(getSaveCourseRequest(id, name, description, language_id));

    return courses.saveCourse(id, name, description, language_id).then(
        response => {
            dispatch({
                type: SAVE_COURSE_SUCCESS,  
                response: normalize(response.data, schema.course),
            });
        },
        error => {
            dispatch({
                type: SAVE_COURSE_FAILURE,
                message: error.message || 'Something went wrong.',
            });
        }
    );
}

export const fetchLanguages = () => (dispatch) => {
    dispatch({ type: FETCH_LANGUAGES_REQUEST });

    return languages.fetchLanguages().then(
        response => {
            dispatch({
                type: FETCH_LANGUAGES_SUCCESS,
                response: normalize(response.data, schema.languagesArray),
            });
        },
        error => {
            dispatch({
                type: FETCH_LANGUAGES_FAILURE,
                message: error.message || 'Something went wrong.',
            });
        }
    );
}

export const fetchCourses = () => (dispatch) => {
    dispatch({ type: FETCH_COURSES_REQUEST });

    return courses.fetchCourses().then(
        response => {
            dispatch({
                type: FETCH_COURSES_SUCCESS,
                response: normalize(response.data, schema.coursesArray),
            });
        },
        error => {
            dispatch({
                type: FETCH_COURSES_FAILURE,
                message: error.message || 'Something went wrong.',
            });
        }
    );
}

export const fetchCourse = (id) => (dispatch) => {
    dispatch({ type: FETCH_COURSE_REQUEST, id });

    return courses.fetchCourse(id).then(
        response => {
            dispatch({
                type: FETCH_COURSE_SUCCESS,
                response: normalize(response.data, schema.course),
            });
        },
        error => {
            dispatch({
                type: FETCH_COURSE_FAILURE,
                message: error.message || 'Something went wrong.',
            });
        }
    );
}

export const status = () => (dispatch) => {
    dispatch({ type: STATUS_REQUEST });

    return auth.status().then(
        response => {
            if (response.data.online) {
                return dispatch({
                    type: STATUS_ONLINE,
                    response: normalize(response.data.user, schema.user),
                });
            }
            else {
                return dispatch({
                    type: STATUS_OFFLINE,
                });
            }
        },
        error => {
            dispatch({
                type: STATUS_FAILURE,
                message: error.message || 'Something went wrong.',
            });
        }
    );
}

export const logout = () => (dispatch) => {
    dispatch({ type: LOGOUT_REQUEST });

    return auth.logout().then(
        response => {
            dispatch({
                type: LOGOUT_SUCCESS
            });
        },
        error => {
            dispatch({
                type: LOGOUT_FAILURE,
                errors: error.response ? (error.response.data || []) : [],
                message: error.message || 'Something went wrong.',
            });
        }
    );
}

export const login = (username, password) => (dispatch) => {
    dispatch(getLoginRequest(username, password));

    return auth.login(username, password).then(
        response => {
            dispatch({
                type: LOGIN_SUCCESS,
                csrf: response.data.csrf,
                response: normalize(response.data.user, schema.user),
            });
        },
        error => {
            dispatch({
                type: LOGIN_FAILURE,
                username,
                errors: error.response ? (error.response.data || []) : [],
                message: error.message || 'Something went wrong.',
            });
        }
    );
};

export const signUp = (username, email, password) => (dispatch) => {
    dispatch(getSignUpRequest(username, email, password));

    return auth.signUp(username, email, password).then(
        response => {
            dispatch({
                type: SIGNUP_SUCCESS,
                csrf: response.data.csrf,
                response: normalize(response.data.user, schema.user),
            });
        },
        error => {
            dispatch({
                type: SIGNUP_FAILURE,
                username,
                errors: error.response ? (error.response.data || []) : [],
                message: error.message || 'Something went wrong.',
            });
        }
    );
};
