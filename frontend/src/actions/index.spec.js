import * as actions from './index';
import * as types from './actionTypes';
import http from '../api/http';
import thunk from 'redux-thunk'
import MockAdapter from 'axios-mock-adapter';
import configureMockStore from 'redux-mock-store';

describe('actions', () => {
    let middlewares = [thunk]
    let mockStore = configureMockStore(middlewares)
    let httpPlainMock;
    let httpSecuredMock;
    let store;
    let errors;

    beforeEach(() => {
        store = mockStore({});
        httpPlainMock = new MockAdapter(http.plain);
        httpSecuredMock = new MockAdapter(http.secured);
        localStorage.clear();
    })

    afterEach(() => {
        httpPlainMock.restore(); 
        httpSecuredMock.restore(); 
    })

    describe('fetchLanguages', () => {
        let languages;
        let normalizedLanguages;

        beforeEach(() => {
            languages = [{ id: 1, code: 'de', name: 'German' }, { id: 2, code: 'cn', name: 'Chinese' }];
            normalizedLanguages = { 
                entities: { 
                    languages: { 
                        '1': { id: 1, code: 'de', name: 'German' }, 
                        '2': { id: 2, code: 'cn', name: 'Chinese' } } }, 
                    result: [ 1, 2 ] };
        })

        describe('when api call returns error', () => {
            beforeEach(() => {
                errors = '{}';
                httpPlainMock.onGet('/languages')
                    .reply(500, errors);
            }) 

            it ('dispatches FETCH_LANGUAGES_FAILURE', () => {
                return store.dispatch(actions.fetchLanguages())
                    .then(() =>
                        {
                            expect(store.getActions())
                                .toEqual([
                                    { type: types.FETCH_LANGUAGES_REQUEST },
                                    { type: types.FETCH_LANGUAGES_FAILURE, message: 'Request failed with status code 500'}
                                ])
                        });
            })
        })

        describe('when api returns successfully', () => {
            beforeEach(() => {
                httpPlainMock.onGet('/languages')
                    .reply(200, languages);
            })

            it ('dispatches FETCH_LANGUAGES_SUCCESS', () => {
                return store.dispatch(actions.fetchLanguages())
                    .then(() =>
                        {
                            expect(store.getActions())
                                .toEqual([
                                    { type: types.FETCH_LANGUAGES_REQUEST },
                                    { type: types.FETCH_LANGUAGES_SUCCESS, response: normalizedLanguages }
                                ])
                        });
            })
        })
    })

    describe('fetchCourse', () => {
        let course;
        let normalizedCourses;

        beforeEach(() => {
            course = { id: 1, language_id: 1, name: 'German', description: 'german course' };
            normalizedCourses = { 
                entities: { 
                    courses: { 
                        '1': { id: 1, language_id: 1, name: 'German', description: 'german course' } 
                    } 
                }, 
                result: 1 
            };
        })

        describe('when api call returns error', () => {
            beforeEach(() => {
                errors = '{}';
                httpPlainMock.onGet('/courses/1')
                    .reply(500, errors);
            }) 

            it ('dispatches FETCH_COURSE_FAILURE', () => {
                return store.dispatch(actions.fetchCourse(1))
                    .then(() =>
                        {
                            expect(store.getActions())
                                .toEqual([
                                    { type: types.FETCH_COURSE_REQUEST, id: 1 },
                                    { type: types.FETCH_COURSE_FAILURE, message: 'Request failed with status code 500'}
                                ])
                        });
            })
        })

        describe('when api returns successfully', () => {
            beforeEach(() => {
                httpPlainMock.onGet('/courses/1')
                    .reply(200, course);
            })

            it ('dispatches FETCH_COURSE_SUCCESS', () => {
                return store.dispatch(actions.fetchCourse(1))
                    .then(() =>
                        {
                            expect(store.getActions())
                                .toEqual([
                                    { type: types.FETCH_COURSE_REQUEST, id: 1 },
                                    { type: types.FETCH_COURSE_SUCCESS, response: normalizedCourses }
                                ])
                        });
            })
        })
    })

    describe('fetchCourses', () => {
        let courses;
        let normalizedCourses;

        beforeEach(() => {
            courses = [{ id: 1, language_id: 1, name: 'German', description: 'german course' }, { id: 2, language_id: 2, name: 'Chinese', description: 'chinese course' }];
            normalizedCourses = { 
                entities: { 
                    courses: { 
                        '1': { id: 1, language_id: 1, name: 'German', description: 'german course' }, 
                        '2': { id: 2, language_id: 2, name: 'Chinese', description: 'chinese course' } } }, 
                    result: [ 1, 2 ] };
        })

        describe('when api call returns error', () => {
            beforeEach(() => {
                errors = '{}';
                httpPlainMock.onGet('/courses')
                    .reply(500, errors);
            }) 

            it ('dispatches FETCH_COURSES_FAILURE', () => {
                return store.dispatch(actions.fetchCourses())
                    .then(() =>
                        {
                            expect(store.getActions())
                                .toEqual([
                                    { type: types.FETCH_COURSES_REQUEST },
                                    { type: types.FETCH_COURSES_FAILURE, message: 'Request failed with status code 500'}
                                ])
                        });
            })
        })

        describe('when api returns successfully', () => {
            beforeEach(() => {
                httpPlainMock.onGet('/courses')
                    .reply(200, courses);
            })

            it ('dispatches FETCH_COURSES_SUCCESS', () => {
                return store.dispatch(actions.fetchCourses())
                    .then(() =>
                        {
                            expect(store.getActions())
                                .toEqual([
                                    { type: types.FETCH_COURSES_REQUEST },
                                    { type: types.FETCH_COURSES_SUCCESS, response: normalizedCourses }
                                ])
                        });
            })
        })
    })

    describe('saveCourse', () => {
        let id = 3;
        let name = 'portuguese';
        let description = 'portuguese course';
        let language_id = 3;

        let normalizedPortugueseCourse = {
            entities: {
                courses: { 
                   '3' : { id, language_id, name, description } 
                } 
            }, 
            result: id 
        };


        describe('when id is not specified', () => {
            beforeEach(() => id = null)

            describe('when api returns successfully', () => {
                beforeEach(() => {
                    httpSecuredMock.onPost('/courses/') 
                        .reply(200, { id: 3, name, description, language_id });
                })

                it('creates SAVE_COURSE_SUCCESS', () => {
                    return store.dispatch(actions.saveCourse(id, name, description, language_id))
                        .then(() => 
                            {
                                expect(store.getActions())
                                    .toEqual([
                                        { type: types.SAVE_COURSE_REQUEST, id, name, description, language_id },
                                        { type: types.SAVE_COURSE_SUCCESS, response: normalizedPortugueseCourse }
                                    ])
                            });
                })
            })
        })

        describe('when id is specified', () => {
            beforeEach(() => id = 3)

            describe('when api call returns error', () => {
                beforeEach(() => {
                    errors = '{}';
                    httpSecuredMock.onPut('/courses/' + id) 
                        .reply(500, errors);
                })

                it('dispatches SAVE_COURSE_FAILURE', () => {
                    return store.dispatch(actions.saveCourse(id, name, description, language_id))
                        .then(() => 
                            {
                                expect(store.getActions())
                                    .toEqual([
                                        { type: types.SAVE_COURSE_REQUEST, id, name, description, language_id }      ,
                                        { type: types.SAVE_COURSE_FAILURE, message: 'Request failed with status code 500'}
                                    ])
                            });
                })
            }) 

            describe('when api returns successfully', () => {
                beforeEach(() => {
                    httpSecuredMock.onPut('/courses/' + id) 
                        .reply(200, { id, name, description, language_id });
                })

                it('creates SAVE_COURSE_SUCCESS', () => {
                    return store.dispatch(actions.saveCourse(id, name, description, language_id))
                        .then(() => 
                            {
                                expect(store.getActions())
                                    .toEqual([
                                        { type: types.SAVE_COURSE_REQUEST, id, name, description, language_id },
                                        { type: types.SAVE_COURSE_SUCCESS, response: normalizedPortugueseCourse }
                                    ])
                            });
                })
            })
        })
    })

    describe('status', () => {
        let user = { id:1, username: 'joe'}
        let normalizedUser = { 
            entities: { 
                users: {
                    '1': user
                },
            },
            result: 1
        }

        describe('when api call returns error', () => {
            beforeEach(() => {
                errors = '{}';
                httpSecuredMock.onGet('/status')
                    .reply(500, errors);
            }) 

            it('dispatches STATUS_FAILURE', () => {
                return store.dispatch(actions.status())
                    .then(() =>
                        {
                            expect(store.getActions())
                                .toEqual([
                                    { type: types.STATUS_REQUEST },
                                    { type: types.STATUS_FAILURE, message: 'Request failed with status code 500'}
                                ])
                        });
            })
        })

        describe('when api returns successfully', () => {
            describe('when user is offline', () => {
                beforeEach(() => {
                    httpSecuredMock.onGet('/status')
                        .reply(200, { online: false });
                }) 

                it('creates STATUS_OFFLINE', () => {
                    return store.dispatch(actions.status())
                        .then(() =>
                            {
                                expect(store.getActions())
                                    .toEqual([
                                        { type: types.STATUS_REQUEST },
                                        { type: types.STATUS_OFFLINE }
                                    ])
                            });
                })
            })

            describe('when user is online', () => {
                beforeEach(() => {
                    httpSecuredMock.onGet('/status')
                        .reply(200, { online: true, user });
                }) 

                it('creates STATUS_ONLINE', () => {
                    return store.dispatch(actions.status())
                        .then(() =>
                            {
                                expect(store.getActions())
                                    .toEqual([
                                        { type: types.STATUS_REQUEST },
                                        { type: types.STATUS_ONLINE, response: normalizedUser }
                                    ])
                            });
                })
            })
        })
    })

    describe('logout', () => {
        describe('when api call returns error', () => {
            beforeEach(() => {
                errors = '{}';
                httpSecuredMock.onDelete('/login')
                    .reply(500, errors);
            }) 

            it('dispatches LOGOUT_FAILURE', () => {
                return store.dispatch(actions.logout())
                    .then(() =>
                        {
                            expect(store.getActions())
                                .toEqual([
                                    { type: types.LOGOUT_REQUEST },
                                    { type: types.LOGOUT_FAILURE, message: 'Request failed with status code 500', errors: JSON.parse(errors) }
                                ])
                        });
            })
        })

        describe('when api returns successfully', () => {
            beforeEach(() => {
                localStorage.setItem('csrf', 'token');

                httpSecuredMock.onDelete('/login')
                    .reply(200);
            }) 

            it('creates LOGOUT_SUCCESS', () => {
                return store.dispatch(actions.logout())
                    .then(() =>
                        {
                            expect(store.getActions())
                                .toEqual([
                                    { type: types.LOGOUT_REQUEST },
                                    { type: types.LOGOUT_SUCCESS }
                                ])
                        });
            })

            it('removes csrf item in local storage', () => {
                return store.dispatch(actions.logout())
                    .then(() =>
                        {
                            expect(localStorage.__STORE__['csrf']).toBeUndefined();
                        });
            })
        })
    })

    describe('login', () => {
        let username = 'joe';
        let password = 'pass';
        let csrf = 'test';
        let message;
        let user = { id:1, username: 'joe'}
        let normalizedUser = { 
            entities: { 
                users: {
                    '1': user
                },
            },
            result: 1
        }

        describe('when api call returns error', () => {
            beforeEach(() => {
                errors = '{"error":"No user found with the email/password provided"}';
                httpPlainMock.onPost('/login', { username, password })
                    .reply(404, errors);
            }) 

            it('dispatches LOGIN_FAILURE', () => {
                return store.dispatch(actions.login(username, password))
                    .then(() =>
                        {
                            expect(store.getActions())
                                .toEqual([
                                    { type: types.LOGIN_REQUEST, username, password },
                                    { type: types.LOGIN_FAILURE, username, message: 'Request failed with status code 404', errors: JSON.parse(errors) }
                                ])
                        });
            })
        })

        describe('when api returns successfully', () => {
            beforeEach(() => {
                httpPlainMock.onPost('/login', { username, password })
                    .reply(200, { csrf: csrf, user });
            }) 

            it('creates LOGIN_SUCCESS', () => {
                return store.dispatch(actions.login(username, password))
                    .then(() =>
                        {
                            expect(store.getActions())
                                .toEqual([
                                    { type: types.LOGIN_REQUEST, username, password },
                                    { type: types.LOGIN_SUCCESS, response: normalizedUser, csrf }
                                ])
                        });
            })

            it('sets csrf item in local storage', () => {
                return store.dispatch(actions.login(username, password))
                    .then(() =>
                        {
                            expect(localStorage.__STORE__['csrf']).toBe(csrf);
                        });
            })
        })
    })

    describe('signUp', () => {
        let username = 'joe';
        let email = 'a@a.org';
        let password = 'pass';
        let csrf = 'test';
        let message;
        let user = { id:1, username: 'joe'}
        let normalizedUser = { 
            entities: { 
                users: {
                    '1': user
                },
            },
            result: 1
        }

        describe('when api call returns error', () => {
            beforeEach(() => {
                errors = '{"username":["has already been taken"],"email":["can\'t be blank"]}';
                httpPlainMock.onPost('/signup', { username, email, password })
                    .reply(422, errors);
            }) 

            it('dispatches SIGNUP_FAILURE', () => {
                return store.dispatch(actions.signUp(username, email, password))
                    .then(() =>
                        {
                            expect(store.getActions())
                                .toEqual([
                                    { type: types.SIGNUP_REQUEST, username, email, password },
                                    { type: types.SIGNUP_FAILURE, username, message: 'Request failed with status code 422', errors: JSON.parse(errors) }
                                ])
                        });
            })
        })

        describe('when api returns successfully', () => {
            beforeEach(() => {
                httpPlainMock.onPost('/signup', { username, email, password })
                    .reply(201, { csrf: csrf, user });
            }) 

            it('creates SIGNUP_SUCCESS', () => {
                return store.dispatch(actions.signUp(username, email, password))
                    .then(() =>
                        {
                            expect(store.getActions())
                                .toEqual([
                                    { type: types.SIGNUP_REQUEST, username, email, password },
                                    { type: types.SIGNUP_SUCCESS, response: normalizedUser, csrf }
                                ])
                        });
            })

            it('sets csrf item in local storage', () => {
                return store.dispatch(actions.signUp(username, email, password))
                    .then(() =>
                        {
                            expect(localStorage.__STORE__['csrf']).toBe(csrf);
                        });
            })
        })
    })
})
