import React from 'react';
import { connect } from 'react-redux';
import { login } from '../actions'
import PropTypes from 'prop-types';
import { getIsRequesting } from '../reducers';

const Login = ({ isRequesting, dispatch }) => {
    let username;
    let password;

    return (
    <form 
        onSubmit={
            e => {
                e.preventDefault();

                if (!username.value.trim()) {
                    return;
                }
                else if (!password.value.trim()) {
                    return; 
                }

                dispatch(login(username.value, password.value));
            }
        }>

        <input id="username" required ref={ node => { username = node; }} type="text" placeholder="Username" />
        <input id="password" required ref={ node => { password = node; }} type="password" placeholder="Password" />

        <button type="submit" disabled={isRequesting}>Login</button>
    </form>
);
}

Login.propTypes = {
    isRequesting: PropTypes.bool.isRequired,
    dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
    return {
        isRequesting: getIsRequesting(state),
    }
}

export default connect(mapStateToProps)(Login);
