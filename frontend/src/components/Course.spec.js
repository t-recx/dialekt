import React from 'react';
import { Provider } from 'react-redux';
import thunk from "redux-thunk";
import configureStore from 'redux-mock-store';
import Course from './Course';
import { FETCH_COURSE_REQUEST } from '../actions/actionTypes';
import { mount, render } from 'enzyme';
import {Route, MemoryRouter, Switch} from 'react-router-dom';

describe('Course', () => {
    let mockStore = configureStore([thunk]);
    let byId;
    let ids = [1, 2];
    let isRequesting;
    let isAdmin;
    let errorMessage = null;
    let store;
    let courseId;
    let userId;

    beforeEach(() => {
        userId = 1;
        courseId = 1;
        isAdmin = false;
        byId = { 
        '1': { id: 1, language_id: 1, name: 'German', description: 'german course', managers: [] }, 
        '2': { id: 2, language_id: 2, name: 'Chinese', description: 'chinese course', managers: [] } };
    })

    const component = () => {
        store = mockStore({ 
            courses: { 
                byId,
                ids, 
                isRequesting,
                errorMessage,
            },
            languages: {
                byId: {},
                ids: [],
                isRequesting: false,
            },
            auth: {
                userId
            },
            users: {
                byId: {
                    '1': { id: userId, admin: isAdmin }
                },
                ids: [1]
            },
        });

        return mount(
            <Provider store={store}>
                <MemoryRouter initialEntries={['/course/' + courseId]}>
                        <Route path="/course/:id" component={Course} />
                </MemoryRouter>
            </Provider>
        );
    }

    describe('when user is not admin', () => {
        beforeEach(() => {
            isAdmin = false;
        }) 

        it('should hide save button', () => {
            expect(component().find('button[children="Save"]').length).toEqual(0);
        })

        it('should not allow editing', () => {
            expect(component().find('input').length).toEqual(0);
        })

        describe('when user is manager', () => {
            beforeEach(() => {
                byId['1'].managers = [1];
            }) 

            it('should show save button', () => {
                expect(component().find('button[children="Save"]').length).toEqual(1);
            })

            it('should allow editing', () => {
                expect(component().find('input').length).not.toEqual(0);
            })
        })
    })

    describe('when user is admin', () => {
        beforeEach(() => {
            isAdmin = true;
        }) 

        it('should show save button', () => {
            expect(component().find('button[children="Save"]').length).toEqual(1);
        })

        it('should allow editing', () => {
            expect(component().find('input').length).not.toEqual(0);
        })
    })

    describe('when course with course id is not in state', () => {
        beforeEach(() => {
            courseId = 9001;
        })

        it('should dispatch FETCH_COURSE_REQUEST action', () => {
            component();
            expect(store.getActions()).toContainEqual({ type: FETCH_COURSE_REQUEST, id: courseId });
        })
    })

    describe('when course with course id is in state', () => {
        beforeEach(() => {
            courseId = 1;
        })

        it('should not dispatch FETCH_COURSE_REQUEST action', () => {
            component();
            expect(store.getActions()).not.toContainEqual({ type: FETCH_COURSE_REQUEST, id: courseId });
        })

        it('should show course data', () => {
            const renderedComponent = component();

            expect(renderedComponent.html()).toContain(byId[courseId.toString()].name)
            expect(renderedComponent.html()).toContain(byId[courseId.toString()].description)
        })
    })

    describe('when is requesting data', () => {
        beforeEach(() => {
            isRequesting = true;
        }) 

        it('should show loading component', () => {
            expect(component().find('Loading').length).toEqual(1); 
        })
    })
});

