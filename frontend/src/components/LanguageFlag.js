import React from "react";
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Loading from './Loading';
import { getLanguagesIds, getLanguage, getLanguagesIsRequesting } from '../reducers'
import { fetchLanguages } from '../actions'

class LanguageFlag extends React.Component {
    componentDidMount() {
        if (!this.props.languagesIds || 
            this.props.languagesIds.length === 0) {
            this.props.dispatch(fetchLanguages());
        }
    }

    render() {
        if (this.props.isRequesting) {
            return (<Loading />);
        }

        return (
            <span>
                <img className="dialekt-hero-foot-flag" src={'/flags/' + this.props.language.code + '.svg'} />
            </span>
        );
    }
}

LanguageFlag.propTypes = {
    value: PropTypes.number,
    language: PropTypes.shape({
        id: PropTypes.number.isRequired,
        code: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
    }),
    languages: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number.isRequired,
            code: PropTypes.string.isRequired,
            name: PropTypes.string.isRequired,
        })
    ),
    isRequesting: PropTypes.bool,
}

const mapStateToProps = (state, ownProps) => {
    let language = { id: 0, code: '', name: ''}
    const languagesIds = getLanguagesIds(state);

    if (languagesIds.includes(ownProps.value)) {
        language = getLanguage(state, ownProps.value) ;
    }

    return {
        language,
        languagesIds,
        isRequesting: getLanguagesIsRequesting(state),
    }
}

export default connect(mapStateToProps)(LanguageFlag);

