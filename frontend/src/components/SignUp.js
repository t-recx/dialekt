import React from 'react';
import { connect } from 'react-redux';
import { signUp } from '../actions'
import PropTypes from 'prop-types';
import { getIsRequesting } from '../reducers';

const SignUp = ({ isRequesting, dispatch }) => {
    let username;
    let email;
    let password;

    return (
    <form 
        onSubmit={
            e => {
                e.preventDefault();

                if (!username.value.trim()) {
                    return;
                }
                else if (!email.value.trim()) {
                    return;
                }
                else if (!password.value.trim()) {
                    return; 
                }

                dispatch(signUp(username.value, email.value, password.value));
            }
        }>

        <input id="username" required ref={ node => { username = node; }} type="text" placeholder="Username" />
        <input id="email" required ref={ node => { email = node; }} type="email" placeholder="E-mail" />
        <input id="password" required ref={ node => { password = node; }} type="password" placeholder="Password" />

        <button type="submit" disabled={isRequesting}>Sign up</button>
    </form>
);
}

SignUp.propTypes = {
    isRequesting: PropTypes.bool.isRequired,
    dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
    return {
        isRequesting: getIsRequesting(state),
    }
}

export default connect(mapStateToProps)(SignUp);
