import React from "react";
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Loading from './Loading';
import { getLanguages, getLanguagesIsRequesting } from '../reducers'
import { fetchLanguages } from '../actions'

class LanguageDropdown extends React.Component {
    componentDidMount() {
        if (!this.props.languages || 
            this.props.languages.length === 0) {
            this.props.dispatch(fetchLanguages());
        }
    }

    render() {
        if (this.props.isRequesting) {
            return (<Loading />);
        }
        
        return (
            <select name={this.props.name} value={this.props.value || ''} onChange={this.props.onChange} required={this.props.required}>
                <option key={0}></option>
                {this.props.languages.map(language => 
                    <option key={language.id} value={language.id}>
                        {language.name}
                    </option>
                )}
            </select>
        );
    }
}

LanguageDropdown.propTypes = {
    value: PropTypes.number,
    languages: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        code: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
    })),
    onChange: PropTypes.func,
    required: PropTypes.bool,
    name: PropTypes.string,
    isRequesting: PropTypes.bool,
}

const mapStateToProps = (state) => {
    return {
        languages: getLanguages(state),
        isRequesting: getLanguagesIsRequesting(state),
    }
}

export default connect(mapStateToProps)(LanguageDropdown);


