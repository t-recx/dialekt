import React from 'react';
import UserAuthBar from './UserAuthBar'
import PropTypes from 'prop-types'
import { getIsCheckingStatus } from '../reducers'
import { connect } from 'react-redux';

const NavBar = ({ isCheckingStatus }) => (
    <nav>
        <header>Linguado</header>

        <aside>
            {!isCheckingStatus &&
            <UserAuthBar signUpRoute='/signup' loginRoute='/login' />
            }
        </aside>
    </nav>
);

NavBar.propTypes = {
  isCheckingStatus: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => {
    return {
        isCheckingStatus: getIsCheckingStatus(state),
    }
}

export default connect(mapStateToProps)(NavBar);
