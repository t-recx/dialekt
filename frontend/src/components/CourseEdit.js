import React from 'react';
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import { saveCourse } from '../actions';
import LanguageDropDown from './LanguageDropdown';

class CourseEdit extends React.Component {
    constructor(props) {
        super(props);
    
        this.state = {
            name: props.name,
            description: props.description,
            language_id: props.language_id,
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        const target = event.target;
        const name = target.name;
        let value = target.value;

        if (name === 'language_id') {
            value = +value;
        }

        this.setState({
            [name]: value    
        });
    }

    handleSubmit(event) {
        event.preventDefault();

        this.props.dispatch(saveCourse(this.props.id, this.state.name, this.state.description, this.state.language_id));
    }

    render () {
        return (
            <form onSubmit={this.handleSubmit}>
                <div>
                    <div>Language</div>

                    <LanguageDropDown name="language_id" value={this.state.language_id} onChange={this.handleChange} required={true} />
                </div>

                <div>
                    <div>Name</div>

                    <input name="name" required value={this.state.name} onChange={this.handleChange} />
                </div>

                <div>
                    <div>Description</div>

                    <textarea name="description" required value={this.state.description} onChange={this.handleChange} >
                    </textarea>
                </div>

                <button type="submit">Save</button> 
            </form>
        );
    }
}

CourseEdit.propTypes = {
    name: PropTypes.string,
    description: PropTypes.string,
    language_id: PropTypes.number,
    dispatch: PropTypes.func.isRequired,
}

export default connect()(CourseEdit);
