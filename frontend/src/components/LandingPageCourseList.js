import React from "react";
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getCourses, getCoursesIsRequesting, getCoursesErrorMessage, getIsAdmin, getLanguagesIsRequesting } from '../reducers';
import { fetchCourses, fetchLanguages } from '../actions';
import Loading from './Loading';
import LanguageFlag from './LanguageFlag'

class LandingPageCourseList extends React.Component {
    componentDidMount() {
        this.props.dispatch(fetchLanguages());
        this.props.dispatch(fetchCourses());
    }

    render () {
        return (
            <div>
                {(this.props.isRequestingCourses || this.props.isRequestingLanguages) ?  
                    <Loading />
                    :
                    <ul>
                        {this.props.courses.map(course => 
                            <li className="has-text-weight-bold " key={course.id}>
                                <Link to={'/course' + course.id}>
                                    <LanguageFlag value={course.language_id} />
                                    {course.name}
                                </Link>
                            </li>
                        )}
                    </ul>
                }
            </div>
        );
    }
}

LandingPageCourseList.propTypes = {
    dispatch: PropTypes.func.isRequired,
    courses: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        language_id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
    })),
    isRequestingCourses: PropTypes.bool,
    isRequestingLanguages: PropTypes.bool,
    isAdmin: PropTypes.bool,
};

const mapStateToProps = (state) => {
    return {
        courses: getCourses(state),
        isRequestingCourses: getCoursesIsRequesting(state),
        isRequestingLanguages: getLanguagesIsRequesting(state),
        errorMessage: getCoursesErrorMessage(state),
        isAdmin: getIsAdmin(state),
    }
}

export default connect(mapStateToProps)(LandingPageCourseList);

