import React from 'react';
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom';
import LandingPageCourseList from './LandingPageCourseList';

export const LandingPage = ({ signUpRoute, loginRoute }) => {
    return (
        <section className="hero is-info is-fullheight">
            <div className="hero-head">
                <nav className="navbar">
                    <div className="container">
                        <div className="navbar-brand">
                            <span className="navbar-burger burger " data-target="navbarMenuHeroB">
                                <span></span>
                                <span></span>
                                <span></span>
                            </span>
                        </div>
                        <div id="navbarMenuHeroB" className="navbar-menu">
                            <div className="navbar-end">
                                <span className="navbar-item">
                                    <Link  to={signUpRoute} className="button is-success">
                                        <span className="icon">
                                            <i className="fas fa-user-plus"></i>
                                        </span>
                                        <span>Sign up</span>
                                    </Link>
                                </span>
                                <span className="navbar-item">
                                    <Link  to={loginRoute} className="button is-info is-inverted">
                                        <span className="icon">
                                            <i className="fas fa-sign-in-alt"></i>
                                        </span>
                                        <span>Login</span>
                                    </Link>
                                </span>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>

            <div className="hero-body">
                <div className="container " align="right">
                    <span className="fas fa-globe-europe fa-10x dialekt-hero-logo">
                    </span>
                </div>
                <div className="container ">
                    <p className="title">
                        Dialekt
                    </p>
                    <p className="subtitle">
                        Connect with the world
                    </p>
                </div>
            </div>

            <div className="hero-foot">
                <nav className="tabs is-boxed is-fullwidth">
                    <div className="container">
                        <LandingPageCourseList />
                    </div>
                </nav>
            </div>
        </section>
        )
}

LandingPage.propTypes = {
    signUpRoute: PropTypes.string.isRequired,
        loginRoute: PropTypes.string.isRequired,
};

export default LandingPage;
