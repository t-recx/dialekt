import React from 'react';
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import history from '../history'
import Course from './Course'
import CourseList from './CourseList'
import Login from './Login'
import SignUp from './SignUp'
import Loading from './Loading'
import PropTypes from 'prop-types'
import LandingPage from './LandingPage'
import UserDashboard from './UserDashboard'
import { status } from '../actions';
import { connect } from 'react-redux';
import { getIsLoggedIn, getUsername, getIsCheckingStatus } from '../reducers';

class App extends React.Component {
    componentDidMount() {
        if (localStorage.getItem('csrf')) {
            this.props.dispatch(status());
        }
    }

    render() {
        return (
            <Router history={history}>

                <Switch>
                    <Route exact path="/">
                        {this.props.isCheckingStatus ? 
                            <Loading />
                            :
                            this.props.isLoggedIn ?
                                <Redirect to={"/users/" + this.props.username} /> 
                                :
                                <LandingPage signUpRoute='/signup' loginRoute='/login' />
                        }
                    </Route>
                    <Route path="/users/:username" component={UserDashboard} />
                    <Route exact path="/login">
                        {this.props.isLoggedIn ? 
                            <Redirect to={"/users/" + this.props.username} /> 
                            :
                            <Login />
                        }
                    </Route>
                    <Route path="/signup" component={SignUp} />
                    <Route path="/course/:id" component={Course} />
                    <Route path="/course" component={Course} />
                    <Route path="/courses" component={CourseList} />
                </Switch>
            </Router>
        );
    }
}

App.propTypes = {
    dispatch: PropTypes.func.isRequired,
    isLoggedIn: PropTypes.bool.isRequired,
    username: PropTypes.string,
    isCheckingStatus: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => {
    return { 
        isLoggedIn: getIsLoggedIn(state),
        username: getUsername(state),
        isCheckingStatus: getIsCheckingStatus(state),
    }
}

export default connect(mapStateToProps)(App);
