import React from 'react';
import { Provider } from 'react-redux';
import thunk from "redux-thunk";
import configureStore from 'redux-mock-store';
import Login from './Login';
import { getLoginRequest } from '../actions';
import { LOGIN_REQUEST } from '../actions/actionTypes';
import { shallow, mount } from 'enzyme';

describe('Login', () => {
    let mockStore = configureStore([thunk]);
    let store;
    let component;
    let username = 'user';
    let password = 'pass';

    beforeEach(() => {
        store = mockStore({auth:{isRequesting:false}});

        component = mount(
            <Provider store={store}>
                <Login />
            </Provider>
        );
    });

    describe('when parameters are correctly filled', () => {
        beforeEach(() => {
            // this works because I'm not attaching any props to these inputs
            // if I was I'd need to simulate a change instead which would update the state
            component.find('#username').instance().value = username;
            component.find('#password').instance().value = password;
        })

        it('should dispatch sign up action on form submit', () => {
            component.find('form').simulate('submit')

            expect(store.getActions()).toContainEqual(getLoginRequest(username, password));
        });
    })

    describe('when isRequesting is true', () => {
        beforeEach(() => {
            store = mockStore({auth: {isRequesting: true}});

            component = mount(
                <Provider store={store}>
                    <Login />
                </Provider>
            );
        }) 

        it('should disable button', () => {
            expect(component.find('button[children="Login"]').instance().disabled).toBeTruthy();
        })
    })
});
