import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getUsername, getIsLoggedIn, getIsRequesting } from '../reducers';
import { logout } from '../actions';

export const UserAuthBar = ({ username, isRequesting, isLoggedIn, logout, signUpRoute, loginRoute }) => {
    return (
        <div>
            {isLoggedIn &&
                <span>
                    <span>
                        {username}
                    </span>
                    <span>
                        <button onClick={logout} disabled={isRequesting}>Sign out</button>
                    </span>
                </span>
            }

            {!isLoggedIn &&
                <span>
                    <Link to={signUpRoute}>Sign up</Link>
                    <Link to={loginRoute}>Login</Link>
                </span>
            }
        </div>
    );
}

UserAuthBar.propTypes = {
    username: PropTypes.string,
    isRequesting: PropTypes.bool.isRequired,
    isLoggedIn: PropTypes.bool.isRequired,
    logout: PropTypes.func.isRequired,
    signUpRoute: PropTypes.string.isRequired,
    loginRoute: PropTypes.string.isRequired,
};

const mapStateToProps = (state) => {
    return {
        username: getUsername(state),
        isLoggedIn: getIsLoggedIn(state),
        isRequesting: getIsRequesting(state),
    }
}

export default connect(mapStateToProps, { logout })(UserAuthBar);
