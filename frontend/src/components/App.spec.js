import React from 'react';
import { Provider } from 'react-redux';
import thunk from "redux-thunk";
import configureStore from 'redux-mock-store';
import App from './App';
import { STATUS_REQUEST } from '../actions/actionTypes';
import { mount } from 'enzyme';

describe('App', () => {
    let mockStore = configureStore([thunk]);
    let store;

    beforeEach(() => {
        localStorage.clear();
        
        store = mockStore({ auth: { isLoggedIn: false, isCheckingStatus: false, isRequesting: false}, languages: { isRequesting: false, ids: [], byId: {} }, courses: { isRequesting: false, ids: [], byId: {} }, users: { ids: [], byId: {} } });
    });

    const component = () => {
        return mount(
            <Provider store={store}>
                <App />
            </Provider>
        );
    }

    describe('when localStorage has csrf token set', () => {
        beforeEach(() => {
            localStorage.setItem('csrf', 'csrftoken');
        })

        it('should dispatch a status request action', () => {
            component();
            expect(store.getActions()).toContainEqual({ type: STATUS_REQUEST });
        });
    })

    describe('when localStorage does not have csrf token set', () => {
        beforeEach(() => {
            localStorage.clear();
        })

        it('should not dispatch a status request action', () => {
            component();
            expect(store.getActions()).not.toContainEqual({ type: STATUS_REQUEST });
        });
    })
});
