import React from "react";
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
    withRouter,
} from "react-router-dom";
import Loading from './Loading';
import CourseView from './CourseView';
import CourseEdit from './CourseEdit';
import { fetchCourse } from '../actions'
import { getIsAdmin, getUserId, getCourse, getCoursesIds, getCoursesIsRequesting } from '../reducers';

class Course extends React.Component {
    constructor(props) {
        super(props);

        this.canEdit = this.canEdit.bind(this);
    }

    componentDidMount() {
        if (this.props.courseId)
        {
            if (!this.props.coursesIds.includes(this.props.courseId)) {
                this.props.dispatch(fetchCourse(this.props.courseId));
            }
        }
    }

    canEdit() {
        return this.props.isAdmin || this.props.course.managers.includes(this.props.userId);
    }

    render() {
        if (this.props.isRequesting) {
            return (<Loading />);
        }
        
        return (
            <div>
                <h3>Course Details</h3>

                {this.canEdit() ?
                    <CourseEdit {...this.props.course} />
                    :
                    <CourseView {...this.props.course} />
                }
            </div>
        );
    }
}

Course.propTypes = {
    courses: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        language_id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        managers: PropTypes.arrayOf(PropTypes.number),
    })),
    isRequesting: PropTypes.bool,
    isAdmin: PropTypes.bool,
    courseId: PropTypes.number,
    userId: PropTypes.number,
}

const mapStateToProps = (state, ownProps) => {
    let course = { name: '', description: '', language_id: null, managers: [] };
    const courseId = isNaN(ownProps.match.params.id) ? null : +ownProps.match.params.id;

    if (getCoursesIds(state).includes(courseId)) {
        course = getCourse(state, courseId);
    }

    return {
        course,
        courseId,
        coursesIds: getCoursesIds(state),
        isRequesting: getCoursesIsRequesting(state),
        isAdmin: getIsAdmin(state),
        userId: getUserId(state),
    }
}

export default withRouter(connect(mapStateToProps)(Course));

