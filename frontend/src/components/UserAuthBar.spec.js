import React from 'react';
import { UserAuthBar } from './UserAuthBar';
import { shallow, mount } from 'enzyme';
import { BrowserRouter as Router } from 'react-router-dom';

describe('UserAuthBar', () => {
    let username;
    let isLoggedIn;
    let handleSignOut;
    let signUpRoute;
    let loginRoute;
    let isRequesting;

    beforeEach(() => {
        username = 'user';
        handleSignOut = jest.fn();
        signUpRoute = '/signup';
        loginRoute = '/signup';
        isRequesting = false;
    });

    describe('when isLoggedIn is true', () => {
        beforeEach(() => {
            isLoggedIn = true; 
        }) 

        describe('when isRequesting is true', () => {
            beforeEach(() => {
                isRequesting = true;
            }) 

            it('should disable signout button', () => {
                expect(component().find('button[children="Sign out"]').instance().disabled).toBeTruthy();
            })
        })

        it('should show username', () => {
            expect(component().html()).toContain(username);
        })

        it('should show sign out button', () => {
            expect(component().find('button').length).toEqual(1);
            expect(component().find('button').text()).toEqual('Sign out');
        })

        describe('when sign out clicked', () => {
            beforeEach(() => {
                component().find('button').simulate('click'); 
            }) 
            
            it('should call sign out callback', () => {
                expect(handleSignOut).toHaveBeenCalled();
            })
        })
    })

    describe('when isLoggedIn is false', () => {
        beforeEach(() => {
            isLoggedIn = false; 
        }) 
    
        it('should not show username', () => {
            expect(component().html()).not.toContain(username)
        })

        it('should show sign up link', () => {
            expect(component().find('Link[children="Sign up"]').props().to).toEqual(signUpRoute)
        })

        it('should show login link', () => {
            expect(component().find('Link[children="Login"]').props().to).toEqual(loginRoute)
        })
    })

    function component() {
        return mount(
            <Router>
                <UserAuthBar 
                    username={username} 
                    isLoggedIn={isLoggedIn}
                    signUpRoute={signUpRoute}
                    isRequesting={isRequesting}
                    logout={handleSignOut}
                    loginRoute={loginRoute}
                />
            </Router>
        );
    }
});
