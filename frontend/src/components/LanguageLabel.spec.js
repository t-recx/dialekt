import React from 'react';
import { Provider } from 'react-redux';
import thunk from "redux-thunk";
import configureStore from 'redux-mock-store';
import LanguageLabel from './LanguageLabel';
import { FETCH_LANGUAGES_REQUEST } from '../actions/actionTypes';
import { mount, render } from 'enzyme';

describe('LanguageLabel', () => {
    let mockStore = configureStore([thunk]);
    let byId = { 
        '1': { id: 1, code: 'de', name: 'German' }, 
        '2': { id: 2, code: 'cn', name: 'Chinese' } 
    };
    let ids = [1, 2];
    let isRequesting;
    let isAdmin;
    let errorMessage = null;
    let store;
    let id;

    beforeEach(() => {
        id = 1;
    })

    const component = () => {
        store = mockStore({ 
            languages: { 
                byId,
                ids, 
                isRequesting,
                errorMessage,
            },
        });

        return mount(
            <Provider store={store}>
                <LanguageLabel value={id}/>
            </Provider>
        );
    }

    it('should not fetch languages when languages already in state', () => {
        component();
        expect(store.getActions()).not.toContainEqual({ type: FETCH_LANGUAGES_REQUEST });
    })

    describe('when languages not in state', () => {
        beforeEach(() => {
            byId = {};
            ids = [];
        })

        it('should dispatch FETCH_LANGUAGES_REQUEST action', () => {
            component();
            expect(store.getActions()).toContainEqual({ type: FETCH_LANGUAGES_REQUEST });
        })
    })

    describe('when is requesting data', () => {
        beforeEach(() => {
            isRequesting = true;
        }) 

        it('should show loading component', () => {
            expect(component().find('Loading').length).toEqual(1); 
        })
    })
});
