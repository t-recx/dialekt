import React from 'react';
import { Provider } from 'react-redux';
import thunk from "redux-thunk";
import configureStore from 'redux-mock-store';
import CourseEdit from './CourseEdit';
import { getSaveCourseRequest } from '../actions';
import { LOGIN_REQUEST } from '../actions/actionTypes';
import { shallow, mount } from 'enzyme';

describe('CourseEdit', () => {
    let mockStore = configureStore([thunk]);
    let byId = { 
        '1': { id: 1, language_id: 1, name: 'German', description: 'german course' }, 
        '2': { id: 2, language_id: 2, name: 'Chinese', description: 'chinese course' } };
    let ids = [1, 2];
    let isRequesting;
    let isAdmin;
    let errorMessage = null;
    let store;
    let component;
    let id = 3;
    let name = 'portuguese course';
    let description = 'description';
    let language_id = 3;
    let userId = 1;

    beforeEach(() => {
        store = mockStore({ 
            courses: { 
                byId,
                ids, 
                isRequesting,
                errorMessage,
            },
            languages: {
                byId: { 
                    '1': { id: 1, name: 'german', code: 'de' },
                    '3': { id: 3, name: 'portuguese', code: 'pt' } 
                },
                ids: [1, 3],
                isRequesting: false,
            },
            auth: {
                userId
            },
            users: {
                byId: {
                    '1': { id: userId, admin: isAdmin }
                },
                ids: [1]
            },
        });

        component = mount(
            <Provider store={store}>
                <CourseEdit id={id} name={name} description={description} language_id={language_id} />
            </Provider>
        );
    });

    describe('when parameters are correctly filled', () => {
        beforeEach(() => {
            name = 'new name';
            description = 'new description';

            component.find('input[name="name"]').simulate('change', { target: { name: 'name', value: name } });
            component.find('textarea[name="description"]').simulate('change', { target: { name: 'description', value: description } });
        })

        it('should dispatch save course action on form submit', () => {
            component.find('form').simulate('submit')

            expect(store.getActions()).toContainEqual(getSaveCourseRequest(id, name, description, language_id));
        });
    })
});

