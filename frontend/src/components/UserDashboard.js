import React from "react";
import {
    useParams,
    //useRouteMatch,
} from "react-router-dom";
import NavBar from './NavBar';

const UserDashboard = () => {
    let { username } = useParams();
    //let { path, url } = useRouteMatch();

    return (
        <div>
            <NavBar />
            
            <h3>Dashboard for { username }</h3>
        </div>
    );
}

export default UserDashboard;
