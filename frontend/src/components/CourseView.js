import React from 'react';
import PropTypes from 'prop-types';
import LanguageLabel from './LanguageLabel';

export const CourseView = ({name, description, language_id}) => {
    return (
        <article>
            <header>{ name }</header>

            <div>
                <LanguageLabel value={language_id} />
            </div>

            <p>
                { description }
            </p>
        </article>
    )
}

CourseView.propTypes = {
    name: PropTypes.string,
    description: PropTypes.string,
}

export default CourseView;
