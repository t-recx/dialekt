import React from "react";
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getCourses, getCoursesIsRequesting, getCoursesErrorMessage, getIsAdmin } from '../reducers';
import { fetchCourses } from '../actions';
import CourseListItem from './CourseListItem';
import Loading from './Loading';

class CourseList extends React.Component {
    componentDidMount() {
        this.props.dispatch(fetchCourses());
    }

    render () {
        return (
            <div>
                {this.props.isRequesting?  
                    <Loading />
                    :
                    <ul>
                        {this.props.courses.map(course => 
                            <CourseListItem key={course.id} {...course} />
                        )}
                    </ul>
                }
                {this.props.isAdmin &&
                    <Link to='/course/'>New</Link>
                }
            </div>
        );
    }
}

CourseList.propTypes = {
    dispatch: PropTypes.func.isRequired,
    courses: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        language_id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
    })),
    isRequesting: PropTypes.bool,
    isAdmin: PropTypes.bool,
};

const mapStateToProps = (state) => {
    return {
        courses: getCourses(state),
        isRequesting: getCoursesIsRequesting(state),
        errorMessage: getCoursesErrorMessage(state),
        isAdmin: getIsAdmin(state),
    }
}

export default connect(mapStateToProps)(CourseList);
