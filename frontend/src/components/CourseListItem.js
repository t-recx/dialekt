import React from "react";
import PropTypes from 'prop-types';
import {
    Link,
} from "react-router-dom";

const CourseListItem = ({id, name, description}) => (
    <li>
        <article>
            <header>
                <Link to={'/course/' + id}>
                    {name}
                </Link>
            </header>
            <p>
                {description}
            </p>
        </article>
    </li>
)

CourseListItem.propTypes = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired
}

export default CourseListItem;
