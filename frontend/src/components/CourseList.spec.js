import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import thunk from "redux-thunk";
import configureStore from 'redux-mock-store';
import CourseList from './CourseList';
import { FETCH_COURSES_REQUEST } from '../actions/actionTypes';
import { mount } from 'enzyme';

describe('CourseList', () => {
    let mockStore = configureStore([thunk]);
    let byId = { 
        '1': { id: 1, language_id: 1, name: 'German', description: 'german course' }, 
        '2': { id: 2, language_id: 2, name: 'Chinese', description: 'chinese course' } };
    let ids = [1, 2];
    let isRequesting;
    let isAdmin;
    let errorMessage = null;
    let store;
    let userId;

    beforeEach(() => {
        userId = 1;
        isRequesting = false;
        isAdmin = false;
    })

    const component = () => {
        store = mockStore({ 
            courses: { 
                byId,
                ids, 
                isRequesting,
                errorMessage,
            },
            auth: {
                userId
            },
            users: {
                byId: {
                    '1': { id: userId, admin: isAdmin }
                },
                ids: [1]
            },
        });
        return mount(
            <Provider store={store}>
                <Router>
                    <CourseList />
                </Router>
            </Provider>
        );
    }

    describe('when user is not admin', () => {
        beforeEach(() => {
            isAdmin = false;
        }) 

        it('should hide new button', () => {
            expect(component().find('Link[children="New"]').length).toEqual(0);
        })
    })

    describe('when user is admin', () => {
        beforeEach(() => {
            isAdmin = true;
        }) 

        it('should show new button', () => {
            expect(component().find('Link[children="New"]').length).toEqual(1);
        })
    })

    describe('when is requesting data', () => {
        beforeEach(() => {
            isRequesting = true;
        }) 

        it('should show loading component', () => {
            expect(component().find('Loading').length).toEqual(1); 
        })
    })

    it('should dispatch a fetch courses action when mounted', () => {
        component();
        expect(store.getActions()).toContainEqual({ type: FETCH_COURSES_REQUEST });
    })

    it('should render every course', () => {
        const renderedComponent = component();

        expect(renderedComponent.html()).toContain(byId['1'].name)
        expect(renderedComponent.html()).toContain(byId['2'].name)
    })
});

