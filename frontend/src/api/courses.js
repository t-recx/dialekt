import http from './http';

export const fetchCourses = () => 
    http.plain
        .get('/courses')
        .then(result => {
            return result;
        })
        .catch(error => {
            throw(error);
        })

export const saveCourse = (id, name, description, language_id) => {
    if (id) {
        return http.secured    
            .put('/courses/' + id, { id, name, description, language_id })
            .then(result => { return result; })
            .catch(error => { throw(error) });
    }
    else {
        return http.secured    
            .post('/courses/', { id, name, description, language_id })
            .then(result => { return result; })
            .catch(error => { throw(error) });
    }
}

export const fetchCourse = (id) => 
    http.plain
        .get('/courses/' + id)
        .then(result => {
            return result;
        })
        .catch(error => {
            throw(error);
        })
