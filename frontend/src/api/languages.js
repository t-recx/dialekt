import http from './http';

export const fetchLanguages = () => 
    http.plain
        .get('/languages')
        .then(result => {
            return result;
        })
        .catch(error => {
            throw(error);
        })


