import http from './http';

export const signUp = (username, email, password) => 
    http.plain
        .post('/signup', {username, email, password})
        .then(result => {
            if (result.data.csrf) {
                // todo try catch here localstorage might not be enabled...
                localStorage.csrf = result.data.csrf;
            }

            return result;
        })
        .catch(error => {
            throw(error);
        })

export const login = (username, password) => 
    http.plain
        .post('/login', {username, password})
        .then(result => {
            if (result.data.csrf) {
                // todo try catch here localstorage might not be enabled...
                localStorage.csrf = result.data.csrf;
            }

            return result;
        })
        .catch(error => {
            throw(error);
        })

export const logout = () => 
    http.secured
        .delete('/login')
        .then(result => {
            // todo try catch here localstorage might not be enabled...
            localStorage.removeItem('csrf');

            return result;
        })
        .catch(error => {
            throw(error);
        })

export const status = () => 
    http.secured
        .get('/status')
        .then(result => {
            return result;
        })
        .catch(error => {
            throw(error);
        })
