import { combineReducers } from 'redux';
import { 
    FETCH_LANGUAGES_FAILURE, FETCH_LANGUAGES_REQUEST, FETCH_LANGUAGES_SUCCESS,
} from '../actions/actionTypes';

const byId = (state = {}, action) => {
    switch(action.type) {
        case FETCH_LANGUAGES_SUCCESS:
            return {...state, ...action.response.entities.languages};
        default:
            return state;
    }
}

const ids = (state = [], action) => {
    switch(action.type) {
        case FETCH_LANGUAGES_SUCCESS:
            return action.response.result;
        default:
            return state;
    }
}

const errorMessage = (state = null, action) => {
    switch(action.type) {
        case FETCH_LANGUAGES_FAILURE:
            return action.message;
        case FETCH_LANGUAGES_SUCCESS:
        case FETCH_LANGUAGES_REQUEST:
            return null;
        default:
            return state;
    }
}

const isRequesting = (state = false, action) => {
    switch(action.type) {
        case FETCH_LANGUAGES_REQUEST:
            return true;
        case FETCH_LANGUAGES_FAILURE:
        case FETCH_LANGUAGES_SUCCESS:
            return false;
        default:
            return state;
    }
}

const languages = combineReducers({
    byId,
    ids,
    isRequesting,
    errorMessage,
});

export default languages;

export const getById = (state) => state.byId;
export const getIds = (state) => state.ids;
export const getIsRequesting = (state) => state.isRequesting;
export const getErrorMessage = (state) => state.errorMessage;
export const getLanguage = (state, id) => state.byId[id];
