import reducer from './auth'
import * as actionTypes from '../actions/actionTypes'
import deepFreeze from 'deep-freeze'

describe('auth reducer', () => {
    let userId = 1
    let username = 'username'
    let email = 'email@email.org'
    let password = 'pass'
    let csrf = 'token'
    let message = 'error'
    let errors = []
    let user = { id: userId, username, email, password }

    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual({
            isLoggedIn: false,
            userId: null,
            errorMessage: null,
            isRequesting: false,
            isCheckingStatus: false,
            errors: [],
        })
    });

    it('should handle SIGNUP_REQUEST', () => {
        const stateBefore = reducer(undefined, {});
        const action = {
            type: actionTypes.SIGNUP_REQUEST,
            username,
            email,
            password
        }
        deepFreeze(stateBefore)
        deepFreeze(action)

        expect(reducer(stateBefore, action))
            .toEqual({
                userId: null,
                errorMessage: null,
                isRequesting: true,
                isLoggedIn: false,
                isCheckingStatus: false,
                errors: [],
            })
    });

    it('should handle SIGNUP_FAILURE', () => {
        const stateBefore = {
            userId: null,
            errorMessage: null,
            isRequesting: true,
            isLoggedIn: false,
            isCheckingStatus: false,
            errors: [],
        }
        const action = {
            type: actionTypes.SIGNUP_FAILURE,
            username,
            errors,
            message
        }
        deepFreeze(stateBefore)
        deepFreeze(action)

        expect(reducer(stateBefore, action))
            .toEqual({
                userId: null,
                errorMessage: message,
                isRequesting: false,
                isLoggedIn: false,
                isCheckingStatus: false,
                errors: errors,
            })
    });

    it('should handle SIGNUP_SUCCESS', () => {
        const stateBefore = {
            userId: null,
            errorMessage: null,
            isRequesting: true,
            isLoggedIn: false,
            isCheckingStatus: false,
            errors: [],
        }
        const action = {
            type: actionTypes.SIGNUP_SUCCESS,
            response: { entities: { users: { '1': {id:1}}}, result: 1},
            csrf
        }
        deepFreeze(stateBefore)
        deepFreeze(action)

        expect(reducer(stateBefore, action))
            .toEqual({
                userId: 1,
                errorMessage: null,
                isRequesting: false,
                isLoggedIn: true,
                isCheckingStatus: false,
                errors: [],
            })
    });

    it('should handle LOGIN_REQUEST', () => {
        const stateBefore = reducer(undefined, {});
        const action = {
            type: actionTypes.LOGIN_REQUEST,
            username,
            password
        }
        deepFreeze(stateBefore)
        deepFreeze(action)

        expect(reducer(stateBefore, action))
            .toEqual({
                userId: null,
                errorMessage: null,
                isRequesting: true,
                isLoggedIn: false,
                isCheckingStatus: false,
                errors: [],
            })
    });

    it('should handle LOGIN_FAILURE', () => {
        const stateBefore = {
            userId: null,
            errorMessage: null,
            isRequesting: true,
            isLoggedIn: false,
            isCheckingStatus: false,
            errors: [],
        }
        const action = {
            type: actionTypes.LOGIN_FAILURE,
            username,
            errors,
            message
        }
        deepFreeze(stateBefore)
        deepFreeze(action)

        expect(reducer(stateBefore, action))
            .toEqual({
                userId: null,
                errorMessage: message,
                isRequesting: false,
                isLoggedIn: false,
                isCheckingStatus: false,
                errors: errors,
            })
    });

    [{ admin: true }, {admin: false}].forEach(testCase => {
        describe('when user is ' + (testCase.admin ? '' : 'not') + ' admin', () => {
            it('should handle LOGIN_SUCCESS', () => {
                const stateBefore = {
                    userId: null,
                    errorMessage: null,
                    isRequesting: true,
                    isLoggedIn: false,
                    isCheckingStatus: false,
                    errors: [],
                }
                const action = {
                    type: actionTypes.LOGIN_SUCCESS,
                    response: { entities: { users: { '1': {id:1, admin: testCase.admin}}}, result: 1},
                    csrf,
                }
                deepFreeze(stateBefore)
                deepFreeze(action)

                expect(reducer(stateBefore, action))
                    .toEqual({
                        userId: 1,
                        errorMessage: null,
                        isRequesting: false,
                        isLoggedIn: true,
                        isCheckingStatus: false,
                        errors: [],
                    })
            })
        }) 
    });

    it('should handle STATUS_REQUEST', () => {
        const stateBefore = reducer(undefined, {});
        const action = {
            type: actionTypes.STATUS_REQUEST,
        }
        deepFreeze(stateBefore)
        deepFreeze(action)

        expect(reducer(stateBefore, action))
            .toEqual({
                userId: null,
                errorMessage: null,
                isRequesting: false,
                isCheckingStatus: true,
                isLoggedIn: false,
                errors: [],
            })
    })

    it('should handle STATUS_FAILURE', () => {
        const stateBefore = {
            userId: null,
            errorMessage: null,
            isRequesting: false,
            isCheckingStatus: true,
            isLoggedIn: false,
            errors: [],
        }
        const action = {
            type: actionTypes.STATUS_FAILURE,
            message
        }
        deepFreeze(stateBefore)
        deepFreeze(action)

        expect(reducer(stateBefore, action))
            .toEqual({
                userId: null,
                errorMessage: message,
                isRequesting: false,
                isCheckingStatus: false,
                isLoggedIn: false,
                errors: [],
            })
    });
       
    it('should handle STATUS_ONLINE', () => {
        const stateBefore = {
            userId: null,
            errorMessage: null,
            isRequesting: false,
            isCheckingStatus: true,
            isLoggedIn: false,
            errors: [],
        }
        const action = {
            type: actionTypes.STATUS_ONLINE,
            response: { entities: { users: { '1': {id:1}}}, result: 1}
        }
        deepFreeze(stateBefore)
        deepFreeze(action)

        expect(reducer(stateBefore, action))
            .toEqual({
                userId: 1,
                errorMessage: null,
                isRequesting: false,
                isCheckingStatus: false,
                isLoggedIn: true,
                errors: [],
            })
    })
       
    it('should handle STATUS_OFFLINE', () => {
        const stateBefore = {
            userId: null,
            errorMessage: null,
            isRequesting: false,
            isCheckingStatus: true,
            isLoggedIn: false,
            errors: [],
        }
        const action = {
            type: actionTypes.STATUS_OFFLINE,
        }
        deepFreeze(stateBefore)
        deepFreeze(action)

        expect(reducer(stateBefore, action))
            .toEqual({
                userId: null,
                errorMessage: null,
                isRequesting: false,
                isCheckingStatus: false,
                isLoggedIn: false,
                errors: [],
            })
    })

    it('should handle LOGOUT_REQUEST', () => {
        const stateBefore = {
            userId: userId,
            errorMessage: null,
            isRequesting: false,
            isLoggedIn: true,
            isCheckingStatus: false,
            errors: [],
        };
        const action = {
            type: actionTypes.LOGOUT_REQUEST
        }
        deepFreeze(stateBefore)
        deepFreeze(action)

        expect(reducer(stateBefore, action))
            .toEqual({
                userId: userId,
                errorMessage: null,
                isRequesting: true,
                isLoggedIn: true,
                isCheckingStatus: false,
                errors: [],
            })
    });

    it('should handle LOGOUT_FAILURE', () => {
        const stateBefore = {
            userId: userId,
            errorMessage: null,
            isRequesting: true,
            isLoggedIn: true,
            isCheckingStatus: false,
            errors: [],
        };
        const action = {
            type: actionTypes.LOGOUT_FAILURE,
            errors,
            message
        }
        deepFreeze(stateBefore)
        deepFreeze(action)

        expect(reducer(stateBefore, action))
            .toEqual({
                userId: userId,
                errorMessage: message,
                isRequesting: false,
                isLoggedIn: true,
                isCheckingStatus: false,
                errors: errors,
            })
    });

    it('should handle LOGOUT_SUCCESS', () => {
        const stateBefore = {
            userId: userId,
            errorMessage: null,
            isRequesting: false,
            isLoggedIn: true,
            isCheckingStatus: false,
            errors: [],
        };
        const action = {
            type: actionTypes.LOGOUT_SUCCESS
        }
        deepFreeze(stateBefore)
        deepFreeze(action)

        expect(reducer(stateBefore, action))
            .toEqual({
                userId: null,
                errorMessage: null,
                isRequesting: false,
                isLoggedIn: false,
                isCheckingStatus: false,
                errors: [],
            })
    });
})
