import { combineReducers } from 'redux';
import { 
    FETCH_COURSES_FAILURE, FETCH_COURSES_REQUEST, FETCH_COURSES_SUCCESS,
    FETCH_COURSE_FAILURE, FETCH_COURSE_REQUEST, FETCH_COURSE_SUCCESS,
    SAVE_COURSE_FAILURE, SAVE_COURSE_REQUEST, SAVE_COURSE_SUCCESS,
} from '../actions/actionTypes';

const byId = (state = {}, action) => {
    switch(action.type) {
        case FETCH_COURSES_SUCCESS:
        case FETCH_COURSE_SUCCESS:
        case SAVE_COURSE_SUCCESS:
            return {...state, ...action.response.entities.courses};
        default:
            return state;
    }
}

const ids = (state = [], action) => {
    switch(action.type) {
        case FETCH_COURSES_SUCCESS:
            return action.response.result;
        case FETCH_COURSE_SUCCESS:
        case SAVE_COURSE_SUCCESS:
            return [...new Set([...state, action.response.result])];
        default:
            return state;
    }
}

const errorMessage = (state = null, action) => {
    switch(action.type) {
        case FETCH_COURSES_FAILURE:
        case FETCH_COURSE_FAILURE:
        case SAVE_COURSE_FAILURE:
            return action.message;
        case FETCH_COURSES_SUCCESS:
        case FETCH_COURSES_REQUEST:
        case FETCH_COURSE_SUCCESS:
        case FETCH_COURSE_REQUEST:
        case SAVE_COURSE_SUCCESS:
        case SAVE_COURSE_REQUEST:
            return null;
        default:
            return state;
    }
}

const isRequesting = (state = false, action) => {
    switch(action.type) {
        case FETCH_COURSES_REQUEST:
        case FETCH_COURSE_REQUEST:
        case SAVE_COURSE_REQUEST:
            return true;
        case FETCH_COURSES_FAILURE:
        case FETCH_COURSES_SUCCESS:
        case FETCH_COURSE_FAILURE:
        case FETCH_COURSE_SUCCESS:
        case SAVE_COURSE_FAILURE:
        case SAVE_COURSE_SUCCESS:
            return false;
        default:
            return state;
    }
}

const courses = combineReducers({
    byId,
    ids,
    isRequesting,
    errorMessage,
});

export default courses;

export const getById = (state) => state.byId;
export const getIds = (state) => state.ids;
export const getIsRequesting = (state) => state.isRequesting;
export const getErrorMessage = (state) => state.errorMessage;
export const getCourse = (state, id) => state.byId[id];
