import reducer from './courses'
import * as actionTypes from '../actions/actionTypes'
import deepFreeze from 'deep-freeze'

describe('courses reducer', () => {
    let message = 'error'
    let ids = [ 1, 2 ]
    let courses = { 
                '1': { id: 1, language_id: 1, name: 'German', description: 'german course' }, 
                '2': { id: 2, language_id: 2, name: 'Chinese', description: 'chinese course' } 
            }
    let singleCourse = { 
                '1': { id: 1, language_id: 1, name: 'German', description: 'german course' }, 
            }
    let response;
    let responseEmpty = { entities: { courses: {} }, result: [] };
    let responseSingle = { entities: { courses: singleCourse }, result: 1 } 

    beforeEach(() => {
        response = { entities: { courses }, result: ids };
    })

    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual({
            byId: {},
            ids: [],
            isRequesting: false,
            errorMessage: null,
        })
    })

    it ('should handle FETCH_COURSES_REQUEST', () => {
        const stateBefore = reducer(undefined, {});
        const action = {
            type: actionTypes.FETCH_COURSES_REQUEST,
        }
        deepFreeze(stateBefore)
        deepFreeze(action)

        expect(reducer(stateBefore, action))
            .toEqual({
                byId: {},
                ids: [],
                isRequesting: true,
                errorMessage: null,
            })
    })

    it('should handle FETCH_COURSES_FAILURE', () => {
        const stateBefore = {
            byId: {},
            ids: [],
            errorMessage: null,
            isRequesting: true,
        }
        const action = {
            type: actionTypes.FETCH_COURSES_FAILURE,
            message
        }
        deepFreeze(stateBefore)
        deepFreeze(action)

        expect(reducer(stateBefore, action))
            .toEqual({
                byId: {},
                ids: [],
                errorMessage: message,
                isRequesting: false,
            })
    })

    it('should handle FETCH_COURSES_SUCCESS', () => {
        const stateBefore = {
            byId: {},
            ids: [],
            errorMessage: null,
            isRequesting: true,
        }
        const action = {
            type: actionTypes.FETCH_COURSES_SUCCESS,
            response
        }
        deepFreeze(stateBefore)
        deepFreeze(action)

        expect(reducer(stateBefore, action))
            .toEqual({
                byId: courses,
                ids: ids,
                errorMessage: null,
                isRequesting: false,
            })
    })

    describe('when response is empty', () => {
        beforeEach(() => {
            response = responseEmpty; 
        })

        it('should handle FETCH_COURSES_SUCCESS', () => {
            let stateBefore = {
                byId: {},
                ids: [],
                errorMessage: null,
                isRequesting: true,
            }
            const action = {
                type: actionTypes.FETCH_COURSES_SUCCESS,
                response
            }
            deepFreeze(stateBefore)
            deepFreeze(action)

            expect(reducer(stateBefore, action))
                .toEqual({
                    byId: {},
                    ids: [],
                    errorMessage: null,
                    isRequesting: false,
                })
        })
    })

    describe('when there are already records present', () => {
        let stateBefore = {}

        beforeEach(() => {
            stateBefore = {
                byId: { '1': courses['1'] },
                ids: [1],
                errorMessage: null,
                isRequesting: true,
            }
            deepFreeze(stateBefore)
        })

        it('should handle FETCH_COURSES_SUCCESS', () => {
            const action = {
                type: actionTypes.FETCH_COURSES_SUCCESS,
                response
            }
            deepFreeze(action)

            expect(reducer(stateBefore, action))
                .toEqual({
                    byId: courses,
                    ids: ids,
                    errorMessage: null,
                    isRequesting: false,
                })
        })

        it('should handle FETCH_COURSE_SUCCESS', () => {
            const action = {
                type: actionTypes.FETCH_COURSE_SUCCESS,
                response: responseSingle
            }
            deepFreeze(action)

            expect(reducer(stateBefore, action))
                .toEqual({
                    byId: singleCourse,
                    ids: [1],
                    errorMessage: null,
                    isRequesting: false,
                })
        })
    })

    it ('should handle FETCH_COURSE_REQUEST', () => {
        const stateBefore = reducer(undefined, {});
        const action = {
            type: actionTypes.FETCH_COURSE_REQUEST,
            id: 1
        }
        deepFreeze(stateBefore)
        deepFreeze(action)

        expect(reducer(stateBefore, action))
            .toEqual({
                byId: {},
                ids: [],
                isRequesting: true,
                errorMessage: null,
            })
    })

    it('should handle FETCH_COURSE_FAILURE', () => {
        const stateBefore = {
            byId: {},
            ids: [],
            errorMessage: null,
            isRequesting: true,
        }
        const action = {
            type: actionTypes.FETCH_COURSE_FAILURE,
            message
        }
        deepFreeze(stateBefore)
        deepFreeze(action)

        expect(reducer(stateBefore, action))
            .toEqual({
                byId: {},
                ids: [],
                errorMessage: message,
                isRequesting: false,
            })
    })

    it ('should handle SAVE_COURSE_REQUEST', () => {
        const stateBefore = reducer(undefined, {});
        const action = {
            type: actionTypes.SAVE_COURSE_REQUEST,
            id: 1
        }
        deepFreeze(stateBefore)
        deepFreeze(action)

        expect(reducer(stateBefore, action))
            .toEqual({
                byId: {},
                ids: [],
                isRequesting: true,
                errorMessage: null,
            })
    })

    it('should handle SAVE_COURSE_FAILURE', () => {
        const stateBefore = {
            byId: {},
            ids: [],
            errorMessage: null,
            isRequesting: true,
        }
        const action = {
            type: actionTypes.SAVE_COURSE_FAILURE,
            message
        }
        deepFreeze(stateBefore)
        deepFreeze(action)

        expect(reducer(stateBefore, action))
            .toEqual({
                byId: {},
                ids: [],
                errorMessage: message,
                isRequesting: false,
            })
    })

    it('should handle SAVE_COURSE_SUCCESS', () => {
        const stateBefore = {
            byId: {},
            ids: [],
            errorMessage: null,
            isRequesting: true,
        }
        const action = {
            type: actionTypes.SAVE_COURSE_SUCCESS,
            response: responseSingle
        }
        deepFreeze(stateBefore)
        deepFreeze(action)

        expect(reducer(stateBefore, action))
            .toEqual({
                byId: singleCourse,
                ids: [1],
                errorMessage: null,
                isRequesting: false,
            })
    })

    it('should handle FETCH_COURSE_SUCCESS', () => {
        const stateBefore = {
            byId: {},
            ids: [],
            errorMessage: null,
            isRequesting: true,
        }
        const action = {
            type: actionTypes.FETCH_COURSE_SUCCESS,
            response: responseSingle
        }
        deepFreeze(stateBefore)
        deepFreeze(action)

        expect(reducer(stateBefore, action))
            .toEqual({
                byId: singleCourse,
                ids: [1],
                errorMessage: null,
                isRequesting: false,
            })
    })
})
