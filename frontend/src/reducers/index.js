import { combineReducers } from 'redux';
import auth, * as fromAuth from './auth';
import courses, * as fromCourses from './courses';
import languages, * as fromLanguages from './languages';
import users, * as fromUsers from './users';

const linguado = combineReducers({
    auth,
    courses,
    languages,
    users,
});

export default linguado;

// auth + users
export const getLoggedUser = (state) => fromUsers.getUser(state.users, fromAuth.getUserId(state.auth));
export const getIsAdmin = (state) => (getLoggedUser(state) || { admin: false }).admin;
export const getUsername = (state) => (getLoggedUser(state) || { username: null }).username;

// auth get
export const getIsLoggedIn = (state) => fromAuth.getIsLoggedIn(state.auth);
export const getIsRequesting = (state) => fromAuth.getIsRequesting(state.auth);
export const getIsCheckingStatus = (state) => fromAuth.getIsCheckingStatus(state.auth);
export const getUserId = (state) => fromAuth.getUserId(state.auth);

// courses get
export const getCourses = (state) => fromCourses.getIds(state.courses).map(id => fromCourses.getCourse(state.courses, id));
export const getCoursesIds = (state) => fromCourses.getIds(state.courses);
export const getCoursesById = (state) => fromCourses.getById(state.courses);
export const getCoursesIsRequesting = (state) => fromCourses.getIsRequesting(state.courses);
export const getCoursesErrorMessage = (state) => fromCourses.getErrorMessage(state.courses);
export const getCourse = (state, id) => fromCourses.getCourse(state.courses, id);

// languages get
export const getLanguages = (state) => fromLanguages.getIds(state.languages).map(id => fromLanguages.getLanguage(state.languages, id));
export const getLanguagesIds = (state) => fromLanguages.getIds(state.languages);
export const getLanguagesById = (state) => fromLanguages.getById(state.languages);
export const getLanguagesIsRequesting = (state) => fromLanguages.getIsRequesting(state.languages);
export const getLanguagesErrorMessage = (state) => fromLanguages.getErrorMessage(state.languages);
export const getLanguage = (state, id) => fromLanguages.getLanguage(state.languages, id);

// users get
export const getUsers = (state) => fromUsers.getIds(state.users).map(id => fromUsers.getUser(state.users, id));
export const getUsersIds = (state) => fromUsers.getIds(state.users);
export const getUsersById = (state) => fromUsers.getById(state.users);
