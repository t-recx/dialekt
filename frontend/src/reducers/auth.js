import { combineReducers } from 'redux';
import { 
    SIGNUP_FAILURE, SIGNUP_REQUEST, SIGNUP_SUCCESS,
    LOGIN_FAILURE, LOGIN_REQUEST, LOGIN_SUCCESS,
    LOGOUT_FAILURE, LOGOUT_REQUEST, LOGOUT_SUCCESS,
    STATUS_FAILURE, STATUS_REQUEST, STATUS_ONLINE, STATUS_OFFLINE,
} from '../actions/actionTypes';

const userId = (state = null, action) => {
    switch (action.type) {
        case SIGNUP_SUCCESS:
        case LOGIN_SUCCESS:
        case STATUS_ONLINE:
            return action.response.result;
        case SIGNUP_FAILURE:
        case SIGNUP_REQUEST:
        case LOGIN_FAILURE:
        case LOGIN_REQUEST:
        case LOGOUT_SUCCESS:
        case STATUS_REQUEST:
        case STATUS_FAILURE:
        case STATUS_OFFLINE:
            return null;
        default:
            return state;
    }
}

const errorMessage = (state = null, action) => {
    switch (action.type) {
        case SIGNUP_FAILURE:
        case LOGIN_FAILURE:
        case LOGOUT_FAILURE:
        case STATUS_FAILURE:
            return action.message;
        case SIGNUP_REQUEST:
        case SIGNUP_SUCCESS:
        case LOGIN_REQUEST:
        case LOGIN_SUCCESS:
        case LOGOUT_REQUEST:
        case LOGOUT_SUCCESS:
        case STATUS_REQUEST:
        case STATUS_ONLINE:
        case STATUS_OFFLINE:
            return null;
        default:
            return state;
    }
}

const isCheckingStatus = (state = false, action) => {
    switch (action.type) {
        case STATUS_REQUEST:
            return true;
        case STATUS_OFFLINE:
        case STATUS_ONLINE:
        case STATUS_FAILURE:
            return false;
        default:
            return state;
    }
}

const isRequesting = (state = false, action) => {
    switch (action.type) {
        case SIGNUP_REQUEST:
        case LOGIN_REQUEST:
        case LOGOUT_REQUEST:
            return true;
        case SIGNUP_SUCCESS:
        case SIGNUP_FAILURE:
        case LOGIN_SUCCESS:
        case LOGIN_FAILURE:
        case LOGOUT_SUCCESS:
        case LOGOUT_FAILURE:
            return false;
        default:
            return state;
    }
}

const isLoggedIn = (state = false, action) => {
    switch (action.type) {
        case SIGNUP_SUCCESS:
        case LOGIN_SUCCESS:
        case STATUS_ONLINE:
            return true;
        case SIGNUP_REQUEST:
        case SIGNUP_FAILURE:
        case LOGIN_REQUEST:
        case LOGIN_FAILURE:
        case LOGOUT_SUCCESS:
        case STATUS_REQUEST:
        case STATUS_OFFLINE:
            return false;
        default:
            return state;
    }
}

const errors = (state = [], action) => {
    switch (action.type) {
        case SIGNUP_SUCCESS:
        case SIGNUP_REQUEST:
        case LOGIN_SUCCESS:
        case LOGIN_REQUEST:
        case LOGOUT_SUCCESS:
        case LOGOUT_REQUEST:
            return [];
        case SIGNUP_FAILURE:
        case LOGIN_FAILURE:
        case LOGOUT_FAILURE:
            return action.errors;
        default:
            return state;
    }
}

const auth = combineReducers({
    userId,
    errorMessage,
    isRequesting,
    isLoggedIn,
    errors,
    isCheckingStatus,
});

export default auth;

export const getUserId = (state) => state.userId;
export const getIsLoggedIn = (state) => state.isLoggedIn;
export const getIsRequesting = (state) => state.isRequesting;
export const getIsCheckingStatus = (state) => state.isCheckingStatus;
