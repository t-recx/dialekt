import { combineReducers } from 'redux';
import { 
    FETCH_COURSES_SUCCESS,
    FETCH_COURSE_SUCCESS,
    SIGNUP_SUCCESS,
    LOGIN_SUCCESS,
    STATUS_ONLINE,
} from '../actions/actionTypes';

const byId = (state = {}, action) => {
    switch(action.type) {
        case FETCH_COURSES_SUCCESS:
        case FETCH_COURSE_SUCCESS:
        case SIGNUP_SUCCESS:
        case LOGIN_SUCCESS:
        case STATUS_ONLINE:
            return {...state, ...action.response.entities.users};
        default:
            return state;
    }
}

const ids = (state = [], action) => {
    switch(action.type) {
        case FETCH_COURSES_SUCCESS:
        case FETCH_COURSE_SUCCESS:
            return [...new Set([...state, ...Object.keys(action.response.entities.users).map(k => +k)])];
        default:
            return state;
    }
}

const users = combineReducers({
    byId,
    ids,
});

export default users;

export const getById = (state) => state.byId;
export const getIds = (state) => state.ids;
export const getUser = (state, id) => state.byId[id];
