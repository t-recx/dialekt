import reducer from './users'
import * as actionTypes from '../actions/actionTypes'
import deepFreeze from 'deep-freeze'

describe('users reducer', () => {
    let joao = { id: 1, name: 'joao' }
    let joaoInDictionary = { '1' : joao }
    let maria = { id: 2, name: 'maria' }
    let ids = [ joao.id, maria.id ]
    let users = {
        '1': joao,
        '2': maria
    }
    let courses = { 
                '1': { id: 1, language_id: 1, name: 'German', description: 'german course', managers: [joao] }, 
                '2': { id: 2, language_id: 2, name: 'Chinese', description: 'chinese course', managers: [joao, maria] } 
            }
    let singleCourse = { 
                '1': { id: 1, language_id: 1, name: 'German', description: 'german course', managers: [joao] }, 
            }
    let response = { entities: { courses, users }, result: ids } 
    let responseSingle = { entities: { courses: singleCourse, users: { '1': joao } }, result: 1 } 
    let responseEmpty = { entities: { courses: {}, users: {}, result: []}}

    beforeEach(() => {
        response = { entities: { courses, users }, result: ids } 
    })

    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual({
            byId: {},
            ids: [],
        })
    })

    it('should handle FETCH_COURSES_SUCCESS', () => {
        const stateBefore = {
            byId: {},
            ids: [],
        }
        const action = {
            type: actionTypes.FETCH_COURSES_SUCCESS,
            response
        }
        deepFreeze(stateBefore)
        deepFreeze(action)

        expect(reducer(stateBefore, action))
            .toEqual({
                byId: users,
                ids: ids,
            })
    })

    describe('when response empty', () => {
        beforeEach(() => {
            response = responseEmpty; 
        })

        it('should handle FETCH_COURSES_SUCCESS', () => {
            const stateBefore = {
                byId: {},
                ids: [],
            }
            const action = {
                type: actionTypes.FETCH_COURSES_SUCCESS,
                response
            }
            deepFreeze(stateBefore)
            deepFreeze(action)

            expect(reducer(stateBefore, action))
                .toEqual({
                    byId: {},
                    ids: [],
                })
        })
    
        it('should handle FETCH_COURSE_SUCCESS', () => {
            const stateBefore = {
                byId: joaoInDictionary,
                ids: [joao.id],
            }
            const action = {
                type: actionTypes.FETCH_COURSE_SUCCESS,
                response
            }
            deepFreeze(action)

            expect(reducer(stateBefore, action))
                .toEqual({
                    byId: joaoInDictionary,
                    ids: [joao.id],
                })
        })
    })

    describe('when there are already records present', () => {
        let stateBefore = {}

        beforeEach(() => {
            stateBefore = {
                byId: joaoInDictionary,
                ids: [joao.id],
            }
            deepFreeze(stateBefore)
        })

        it('should handle FETCH_COURSES_SUCCESS', () => {
            const action = {
                type: actionTypes.FETCH_COURSES_SUCCESS,
                response
            }
            deepFreeze(action)

            expect(reducer(stateBefore, action))
                .toEqual({
                    byId: users,
                    ids: ids,
                })
        })

        it('should handle FETCH_COURSE_SUCCESS', () => {
            const action = {
                type: actionTypes.FETCH_COURSE_SUCCESS,
                response: responseSingle
            }
            deepFreeze(action)

            expect(reducer(stateBefore, action))
                .toEqual({
                    byId: joaoInDictionary,
                    ids: [joao.id],
                })
        })
    })

    it('should handle FETCH_COURSE_SUCCESS', () => {
        const stateBefore = {
            byId: {},
            ids: [],
       }
        const action = {
            type: actionTypes.FETCH_COURSE_SUCCESS,
            response: responseSingle
        }
        deepFreeze(stateBefore)
        deepFreeze(action)

        expect(reducer(stateBefore, action))
            .toEqual({
                byId: joaoInDictionary,
                ids: [joao.id],
            })
    })
})

