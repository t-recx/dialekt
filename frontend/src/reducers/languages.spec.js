import reducer from './languages'
import * as actionTypes from '../actions/actionTypes'
import deepFreeze from 'deep-freeze'

describe('languages reducer', () => {
    let message = 'error'
    let ids = [ 1, 2 ]
    let languages = { 
                '1': { id: 1, code: 'de', name: 'German' }, 
                '2': { id: 2, code: 'cn', name: 'Chinese' } 
            }
    let response = { entities: { languages }, result: ids } 

    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual({
            byId: {},
            ids: [],
            isRequesting: false,
            errorMessage: null,
        })
    })

    it ('should handle FETCH_LANGUAGES_REQUEST', () => {
        const stateBefore = reducer(undefined, {});
        const action = {
            type: actionTypes.FETCH_LANGUAGES_REQUEST,
        }
        deepFreeze(stateBefore)
        deepFreeze(action)

        expect(reducer(stateBefore, action))
            .toEqual({
                byId: {},
                ids: [],
                isRequesting: true,
                errorMessage: null,
            })
    })

    it('should handle FETCH_LANGUAGES_FAILURE', () => {
        const stateBefore = {
            byId: {},
            ids: [],
            errorMessage: null,
            isRequesting: true,
        }
        const action = {
            type: actionTypes.FETCH_LANGUAGES_FAILURE,
            message
        }
        deepFreeze(stateBefore)
        deepFreeze(action)

        expect(reducer(stateBefore, action))
            .toEqual({
                byId: {},
                ids: [],
                errorMessage: message,
                isRequesting: false,
            })
    })

    it('should handle FETCH_LANGUAGES_SUCCESS', () => {
        const stateBefore = {
            byId: {},
            ids: [],
            errorMessage: null,
            isRequesting: true,
        }
        const action = {
            type: actionTypes.FETCH_LANGUAGES_SUCCESS,
            response
        }
        deepFreeze(stateBefore)
        deepFreeze(action)

        expect(reducer(stateBefore, action))
            .toEqual({
                byId: languages,
                ids: ids,
                errorMessage: null,
                isRequesting: false,
            })
    })
})

