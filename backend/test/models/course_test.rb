require 'test_helper'

class CourseTest < ActiveSupport::TestCase
  test "name should be unique" do
    a = Course.new
    a.name = 'test'
    a.language = languages(:one)
    a.description = 'test'

    b = Course.new
    b.name = 'test'
    b.language = languages(:one)
    b.description = 'test'

    assert a.save
    assert_not b.save
  end

  test "name should be stripped before saved and validated before save if equal to existing" do
    a = Course.new
    a.name = '  test     '
    a.language = languages(:one)
    a.description = 'test'

    b = Course.new
    b.name = '     test      '
    b.language = languages(:one)
    b.description = 'test'

    assert a.save
    assert_not b.save
    assert_equal a.name, 'test'
  end

  test "name should have a value" do 
    a = Course.new
    a.name = nil
    a.language = languages(:one)
    a.description = 'test'

    assert_not a.save

    b = Course.new
    b.name = ''
    b.language = languages(:one)
    b.description = 'test'

    assert_not b.save
  end

  test "courses can have users enrolled" do 
    courses_before_user_one = users(:one).courses.length
    courses_before_user_two = users(:two).courses.length
    a = Course.new
    a.name = 'german'
    a.language = languages(:one)
    a.description = 'test'
    a.users.push users(:one)
    a.users.push users(:two)

    assert a.save

    user_one = User.find_by(username:users(:one).username)
    user_two = User.find_by(username:users(:two).username)

    assert_equal user_one.courses.length, courses_before_user_one + 1
    assert_equal user_two.courses.length, courses_before_user_two + 1
  end

  test "courses can't have more than one instance of the user for each course" do
    a = Course.new
    a.name = 'german'
    a.description = 'test'
    a.language = languages(:one)
    a.users.push users(:one)

    b = Course.new
    b.name = 'english'
    b.language = languages(:one)
    b.description = 'test'
    b.users.push users(:one)

    c = Course.new
    c.name = 'chinese'
    c.language = languages(:one)
    c.description = 'test'
    c.users.push users(:one)
    c.users.push users(:one)

    assert a.save
    assert b.save # we can have the same user but on a different course
    assert_not c.save
  end

  test "courses can have managers" do 
    courses_before_user_one = users(:one).managed_courses.length
    courses_before_user_two = users(:two).managed_courses.length
    a = Course.new
    a.name = 'german'
    a.language = languages(:one)
    a.description = 'test'
    a.managers.push users(:one)
    a.managers.push users(:two)

    assert a.save

    user_one = User.find_by(username:users(:one).username)
    user_two = User.find_by(username:users(:two).username)

    assert_equal user_one.managed_courses.length, courses_before_user_one + 1
    assert_equal user_two.managed_courses.length, courses_before_user_two + 1
  end

  test "courses can't have more than one instance of the manager for each course" do
    a = Course.new
    a.name = 'german'
    a.language = languages(:one)
    a.description = 'test'
    a.managers.push users(:one)

    b = Course.new
    b.name = 'english'
    b.language = languages(:one)
    b.description = 'test'
    b.managers.push users(:one)

    c = Course.new
    c.name = 'chinese'
    c.language = languages(:one)
    c.description = 'test'
    c.managers.push users(:one)
    c.managers.push users(:one)

    assert a.save
    assert b.save # we can have the same user but on a different course
    assert_not c.save
  end
end
