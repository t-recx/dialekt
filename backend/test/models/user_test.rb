require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "username should be unique" do
    a = User.new
    a.username = 'test'
    a.email = 'test@email.org'
    a.password_digest = 'aaaa'

    b = User.new
    b.username = 'test'
    b.email = 'test2@email.org'
    b.password_digest = 'aaaa'

    assert a.save
    assert_not b.save
  end

  test "email should be unique" do
    a = User.new
    a.username = 'test'
    a.email = 'test@test.com'
    a.password_digest = 'aaaa'

    b = User.new
    b.username = 'test2'
    b.email = 'test@test.com'
    b.password_digest = 'aaaa'

    assert a.save
    assert_not b.save
  end

  test "username should be stripped before save and validated if equal to existing" do
    a = User.new
    a.username = 'test'
    a.email = 'test@test.org'
    a.password_digest = 'aaaa'

    b = User.new
    b.username = '   test '
    a.email = 'test2@test.org'
    b.password_digest = 'aaaa'

    assert a.save
    assert_not b.save
  end

  test "email should be stripped before save and validated if equal to existing" do
    a = User.new
    a.username = 'test'
    a.email = 'test@test.org'
    a.password_digest = 'aaaa'

    b = User.new
    b.username = 'test2'
    b.email = '   test@test.org   '
    b.password_digest = 'aaaa'

    assert a.save
    assert_not b.save
  end

  test "users can be part of courses" do 
    users_before_course_one = courses(:one).users.length
    users_before_course_two = courses(:two).users.length

    a = User.new
    a.username = 'test'
    a.email = 'test@test.org'
    a.password_digest = 'aaa'
    a.courses.push courses(:one)
    a.courses.push courses(:two)

    assert a.save

    one = Course.find_by(name:courses(:one).name)
    two = Course.find_by(name:courses(:two).name)
    assert_equal one.users.length, users_before_course_one + 1
    assert_equal two.users.length, users_before_course_two + 1
  end

  test "users can't have more than one instance of the same course" do
    a = User.new
    a.username = 'test'
    a.email = 'test@test.org'
    a.password_digest = 'aaa'
    a.courses.push courses(:one)
    a.courses.push courses(:one)

    assert_not a.save
  end

  test "users can be course managers" do 
    users_before_course_one = courses(:one).managers.length
    users_before_course_two = courses(:two).managers.length

    a = User.new
    a.username = 'test'
    a.email = 'test@test.org'
    a.password_digest = 'aaa'
    a.managed_courses.push courses(:one)
    a.managed_courses.push courses(:two)

    assert a.save

    one = Course.find_by(name:courses(:one).name)
    two = Course.find_by(name:courses(:two).name)
    assert_equal one.managers.length, users_before_course_one + 1
    assert_equal two.managers.length, users_before_course_two + 1
  end

  test "users can't have more than one instance of the same managed course" do
    a = User.new
    a.username = 'test'
    a.email = 'test@test.org'
    a.password_digest = 'aaa'
    a.managed_courses.push courses(:one)
    a.managed_courses.push courses(:one)

    assert_not a.save
  end
end
