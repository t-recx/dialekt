require 'test_helper'

class EnrolmentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @enrolment = enrolments(:one)
  end

  test "should get index" do
    get enrolments_url, as: :json
    assert_response :success
  end

  test "should create enrolment" do
    assert_difference('Enrolment.count') do
      post enrolments_url, params: { enrolment: { user_id: users(:one).id, course_id: courses(:two).id  } }, as: :json
    end

    assert_response 201
  end

  test "should not create enrolment when association already exists" do
    assert_no_difference('User.count') do 
      post enrolments_url, params: { enrolment: { user_id: enrolments(:one).user_id, course_id: enrolments(:one).course_id  } }, as: :json
    end

    assert_response 422
    assert_match '"user_id":["has already been taken"]', @response.body
  end

  test "should show enrolment" do
    get enrolment_url(@enrolment), as: :json
    assert_response :success
  end

  test "should destroy enrolment" do
    assert_difference('Enrolment.count', -1) do
      delete enrolment_url(@enrolment), as: :json
    end

    assert_response 204
  end
end
