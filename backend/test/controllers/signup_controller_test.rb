require 'test_helper'

class SignUpControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = { username: 'test', password: 'test', email: 'test@test.org' }
    @existing_user = users(:one)
  end

  test "should create user" do
    assert_difference('User.count') do
      post signup_url, params: { email: @user[:email], password: @user[:password], username: @user[:username] }, as: :json
    end

    assert_response 201
    assert JSON.parse(@response.body).keys.any? { |x| x == 'csrf' }
  end

  test "created user should always have admin equal false even if parameter is passed" do 
    assert_difference('User.count') do
      post signup_url, params: { email: @user[:email], password: @user[:password], username: @user[:username], admin:true }, as: :json
    end

    assert_response 201

    assert_equal User.find_by(email:@user[:email]).admin, false
  end

  test "should not create user if username already exists" do
    assert_no_difference('User.count') do 
      post signup_url, params: { username: @existing_user.username, email: @user[:email], password: @user[:password] }, as: :json
    end

    assert_response 422
    assert_match '"username":["has already been taken"]', @response.body
  end

  test "should not create user if email already exists" do
    assert_no_difference('User.count') do 
      post signup_url, params: { email: @existing_user.email, password: @user[:password], username: @user[:username] }, as: :json
    end

    assert_response 422
    assert_match '"email":["has already been taken"]', @response.body
  end
end
