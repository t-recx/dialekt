require 'test_helper'

class CoursesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @course = courses(:one)

    setup_users
  end

  def setup_users
    @user = { username: 'test', password: 'test', email: 'test@test.org' }
    user = User.new({username: @user[:username], email: @user[:email], password: @user[:password], admin:false})
    user.save

    @teacher_user = { username: 'teacher', password: 'test', email: 'teacher@test.org' }
    teacher_user = User.new({username: @teacher_user[:username], email: @teacher_user[:email], password: @teacher_user[:password], admin:false})
    teacher_user.save
    @course.managers.push teacher_user
    @course.save

    @admin_user = { username: 'admin', password: 'admin', email: 'admin@test.org' }
    admin_user = User.new({username: @admin_user[:username], email: @admin_user[:email], password: @admin_user[:password], admin:true})
    admin_user.save
  end

  def login user
    post login_url, params: { username: user[:username], password: user[:password] }, as: :json

    cookies = @response.cookies

    @headers = { JWTSessions.csrf_header => JSON.parse(@response.body)['csrf'] }
  end

  test "should get index" do
    get courses_url, as: :json
    assert_response :success
  end

  test "should create course when logged-in user is an admin" do
    login @admin_user

    assert_difference('Course.count') do
      create
    end

    assert_response 201
  end

  test "should not create course when logged-in user is not an admin" do
    login @user

    assert_no_difference('Course.count') do
      create
    end

    assert_response 403
  end

  test "should not create course when user is not logged" do
    assert_no_difference('Course.count') do
      create
    end

    assert_response :unauthorized
  end

  test "should show course" do
    get course_url(@course), as: :json
    assert_response :success
  end

  test "should return unauthorized if trying to update and not logged in" do
    patch course_url(@course), params: { course: { language_id: @course.language_id, name: @course.name } }, as: :json

    assert_response :unauthorized
  end

  test "should update course if user is admin" do
    login @admin_user

    update

    assert_response :success
  end

  test "should update course if user is teacher" do
    login @teacher_user

    update

    assert_response :success
  end

  test "should not update course if user is not a teacher nor an admin" do
    login @user

    update

    assert_response :forbidden
  end

  test "should not update course if user is not logged in" do
    update

    assert_response :unauthorized
  end

  test "should destroy course if user is admin" do
    login @admin_user

    assert_difference('Course.count', -1) do
      destroy
    end

    assert_response 204
  end

  test "should not destroy course if user is not admin" do
    login @teacher_user

    assert_no_difference('Course.count') do
      destroy
    end

    assert_response :forbidden
  end

  test "should throw unauthorize on destroy course if user is not logged" do
    assert_no_difference('Course.count') do
      destroy
    end

    assert_response :unauthorized
  end

  def create
    post courses_url, headers: @headers, params: { course: { language_id: @course.language_id, name: 'new name', description: 'test' } }, as: :json
  end

  def update
    patch course_url(@course), headers: @headers, params: { course: { language_id: @course.language_id, name: @course.name } }, as: :json
  end

  def destroy
    delete course_url(@course), headers: @headers, as: :json
  end
end
