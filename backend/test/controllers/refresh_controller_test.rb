require 'test_helper'

class RefreshControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = User.last
  end

  test "when token expired should refresh" do
      JWTSessions.access_exp_time = 0
      payload = { user_id: @user.id }
      session = JWTSessions::Session.new(payload: payload, refresh_by_access_allowed: true)
      @tokens = session.login
      JWTSessions.access_exp_time = 3600

      cookies[JWTSessions.access_cookie] = @tokens[:access]

      post refresh_url, headers: { JWTSessions.csrf_header => @tokens[:csrf] }

      assert_response :success
      assert JSON.parse(@response.body).keys.any? { |x| x == 'csrf' }
      assert response.cookies[JWTSessions.access_cookie]
  end

  test "when token still valid should not refresh" do
      payload = { user_id: @user.id }
      session = JWTSessions::Session.new(payload: payload, refresh_by_access_allowed: true)
      @tokens = session.login

      cookies[JWTSessions.access_cookie] = @tokens[:access]

      post refresh_url, headers: { JWTSessions.csrf_header => @tokens[:csrf] }

      assert_response 401
  end
end
