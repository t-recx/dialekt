require 'test_helper'

class TeachershipsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @teachership = teacherships(:one)
  end

  test "should get index" do
    get teacherships_url, as: :json
    assert_response :success
  end

  test "should create teachership" do
    assert_difference('Teachership.count') do
      post teacherships_url, params: { teachership: { user_id: users(:one).id, course_id: courses(:two).id  } }, as: :json
    end

    assert_response 201
  end

  test "should not create teachership when association already exists" do
    assert_no_difference('User.count') do 
      post teacherships_url, params: { teachership: { user_id: teacherships(:one).user_id, course_id: teacherships(:one).course_id  } }, as: :json
    end

    assert_response 422
    assert_match '"user_id":["has already been taken"]', @response.body
  end

  test "should show teachership" do
    get teachership_url(@teachership), as: :json
    assert_response :success
  end

  test "should destroy teachership" do
    assert_difference('Teachership.count', -1) do
      delete teachership_url(@teachership), as: :json
    end

    assert_response 204
  end
end
