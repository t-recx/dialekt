require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:one)
  end

  test "should get index" do
    get users_url, as: :json
    assert_response :success
  end

  test "should show user" do
    get user_url(@user), as: :json
    assert_response :success
  end

  test "no passwords should come on index" do
    get users_url, as: :json
    assert JSON.parse(response.body).none? { |a| a.keys.any? { |x| x.include? "password"} }
  end

  test "no passwords should come on show user" do
    get user_url(@user), as: :json
    assert JSON.parse(response.body).keys.none? { |x| x.include? "password"} 
  end
end
