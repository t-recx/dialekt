require 'test_helper'

class LoginControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = { username: 'test', password: 'test', email: 'test@test.org' }
  end

  test "should login user" do
    post signup_url, params: { email: @user[:email], password: @user[:password], username: @user[:username] }, as: :json

    assert_response :created

    post login_url, params: { username: @user[:username], password: @user[:password] }, as: :json

    assert_response :success

    assert JSON.parse(@response.body).keys.any? { |x| x == 'csrf' }
    assert JSON.parse(@response.body).keys.any? { |x| x == 'user' }
    assert_equal JSON.parse(@response.body)['user']['admin'], false
  end

  test "should return admin depending on user field" do
    post signup_url, params: { email: @user[:email], password: @user[:password], username: @user[:username] }, as: :json

    assert_response :created

    user = User.find_by(username: @user[:username]) 
    user.admin = true
    user.save

    post login_url, params: { username: @user[:username], password: @user[:password] }, as: :json

    assert_equal JSON.parse(@response.body)['user']['admin'], true
  end

  test "should not login if wrong password" do
    post signup_url, params: { email: @user[:email], password: @user[:password], username: @user[:username] }, as: :json

    assert_response :created

    post login_url, params: { username: @user[:username], password: 'wrong_password' }, as: :json

    assert_response :unauthorized
  end

  test "logout should logout if user logged in correctly" do
    post signup_url, params: { email: @user[:email], password: @user[:password], username: @user[:username] }, as: :json
    post login_url, params: { username: @user[:username], password: @user[:password] }, as: :json
    assert_response :success
    cookies = @response.cookies

    delete login_url, headers: { JWTSessions.csrf_header => JSON.parse(@response.body)['csrf'] }

    assert_response :success
    post login_url, params: { username: @user[:username], password: @user[:password] }, as: :json
    assert_response :success
  end

  test "logout should do nothing if user is not currently logged in" do
    post signup_url, params: { email: @user[:email], password: @user[:password], username: @user[:username] }, as: :json
    post login_url, params: { username: @user[:username], password: @user[:password] }, as: :json
    cookies = @response.cookies
    delete login_url, headers: { JWTSessions.csrf_header => JSON.parse(@response.body)['csrf'] }
    assert_response :success

    delete login_url, headers: { JWTSessions.csrf_header => JSON.parse(@response.body)['csrf'] }

    assert_response :unauthorized
  end
end
