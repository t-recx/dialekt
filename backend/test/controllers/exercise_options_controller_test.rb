require 'test_helper'

class ExerciseOptionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @exercise_option = exercise_options(:one)
  end

  test "should get index" do
    get exercise_options_url, as: :json
    assert_response :success
  end

  test "should create exercise_option" do
    assert_difference('ExerciseOption.count') do
      post exercise_options_url, params: { exercise_option: { correct: @exercise_option.correct, exercise_id: @exercise_option.exercise_id, option: @exercise_option.option } }, as: :json
    end

    assert_response 201
  end

  test "should show exercise_option" do
    get exercise_option_url(@exercise_option), as: :json
    assert_response :success
  end

  test "should update exercise_option" do
    patch exercise_option_url(@exercise_option), params: { exercise_option: { correct: @exercise_option.correct, exercise_id: @exercise_option.exercise_id, option: @exercise_option.option } }, as: :json
    assert_response 200
  end

  test "should destroy exercise_option" do
    assert_difference('ExerciseOption.count', -1) do
      delete exercise_option_url(@exercise_option), as: :json
    end

    assert_response 204
  end
end
