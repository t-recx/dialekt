Rails.application.routes.draw do
  resources :enrolments
  resources :teacherships
  resources :exercise_options
  resources :exercises
  resources :lessons

  resources :courses do
    resources :teacherships
  end

  resources :languages
  resources :users
  
  get 'status', controller: :status, action: :index
  post 'refresh', controller: :refresh, action: :create
  post 'login', controller: :login, action: :create
  post 'signup', controller: :signup, action: :create
  delete 'login', controller: :login, action: :destroy

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
