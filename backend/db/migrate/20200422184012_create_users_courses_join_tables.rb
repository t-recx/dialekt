class CreateUsersCoursesJoinTables < ActiveRecord::Migration[6.0]
  def change
    create_table :users_courses, id: false do |t|
      t.belongs_to :users
      t.belongs_to :courses
    end
  end
end
