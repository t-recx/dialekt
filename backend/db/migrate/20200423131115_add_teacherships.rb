class AddTeacherships < ActiveRecord::Migration[6.0]
  def change
    create_table :teacherships do |t|
      t.belongs_to :user
      t.belongs_to :course

      t.timestamps
    end

    add_index :teacherships, [:user_id, :course_id], unique: true
  end
end
