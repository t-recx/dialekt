class RemoveFlagCodeFromCourse < ActiveRecord::Migration[6.0]
  def change

    remove_column :courses, :flag_code, :string
  end
end
