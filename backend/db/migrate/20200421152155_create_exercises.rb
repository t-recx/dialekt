class CreateExercises < ActiveRecord::Migration[6.0]
  def change
    create_table :exercises do |t|
      t.integer :type, default: 0
      t.string :prompt
      t.references :lesson, null: false, foreign_key: true

      t.timestamps
    end
  end
end
