class ChangeColumnTypeToExerciseTypeOnTableExercises < ActiveRecord::Migration[6.0]
  def change
    change_table :exercises do |t|
      t.rename :type, :exercise_type
    end
  end
end
