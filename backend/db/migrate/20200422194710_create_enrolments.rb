class CreateEnrolments < ActiveRecord::Migration[6.0]
  def change
    drop_table :courses_users

    create_table :enrolments do |t|
      t.belongs_to :user
      t.belongs_to :course

      t.timestamps
    end

    add_index :enrolments, [:user_id, :course_id], unique: true
  end
end
