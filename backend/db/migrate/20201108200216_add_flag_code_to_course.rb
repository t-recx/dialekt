class AddFlagCodeToCourse < ActiveRecord::Migration[6.0]
  def change
    add_column :courses, :flag_code, :string
  end
end
