class ChangeUsersCoursesTableToCoursesUsers < ActiveRecord::Migration[6.0]
  def change
    rename_table :users_courses, :courses_users
  end
end
