class FixCoursesUsersTable < ActiveRecord::Migration[6.0]
  def change
    drop_table :courses_users

    create_table :courses_users, id: false do |t|
      t.belongs_to :user
      t.belongs_to :course
    end
  end
end
