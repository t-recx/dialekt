class Exercise < ApplicationRecord
  belongs_to :lesson
  has_many :exercise_options, dependent: :destroy

  enum exercise_type: [:choose, :select, :translate], _prefix: :exercise_type
end
