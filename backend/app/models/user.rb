class User < ApplicationRecord
  strip_attributes only: [:username, :email]

  validates :username, uniqueness: true, allow_blank: false, presence: true
  validates :email, uniqueness: { case_sensitive: false }, allow_blank: false, presence: true

  has_secure_password

  has_many :enrolments
  has_many :teacherships
  has_many :courses, through: :enrolments
  has_many :managed_courses, through: :teacherships, source: :course
end
