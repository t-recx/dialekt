class Course < ApplicationRecord
  strip_attributes only: [:name]

  belongs_to :language

  has_many :lessons, dependent: :destroy

  validates :name, uniqueness: true, allow_blank: false, presence: true

  has_many :enrolments
  has_many :teacherships
  has_many :users, through: :enrolments
  has_many :managers, through: :teacherships, source: :user
end
