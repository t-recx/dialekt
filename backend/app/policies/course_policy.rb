class CoursePolicy < ApplicationPolicy
    def create?
        user.admin?
    end

    def update?
        user.admin? or user.managed_courses.include?(record)
    end

    def destroy?
        user.admin?
    end
end