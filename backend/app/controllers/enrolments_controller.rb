class EnrolmentsController < ApplicationController
  before_action :set_enrolment, only: [:show, :destroy]

  # GET /enrolments
  def index
    @enrolments = Enrolment.all

    render json: @enrolments
  end

  # GET /enrolments/1
  def show
    render json: @enrolment
  end

  # POST /enrolments
  def create
    @enrolment = Enrolment.new(enrolment_params)

    if @enrolment.save
      render json: @enrolment, status: :created, location: @enrolment
    else
      render json: @enrolment.errors, status: :unprocessable_entity
    end
  end

  # DELETE /enrolments/1
  def destroy
    @enrolment.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_enrolment
      @enrolment = Enrolment.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def enrolment_params
      params.require(:enrolment).permit(:user_id, :course_id)
    end
end
