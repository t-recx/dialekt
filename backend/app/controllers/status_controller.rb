class StatusController < ApplicationController
  before_action :authorize_access_request!

  rescue_from JWTSessions::Errors::Unauthorized, with: :not_online
  
  def index
    render json: { online: true, user: current_user.as_json.except('password_digest') }, status: :ok
  end

  private

  def not_online
    render json: { online: false }, status: :ok
  end
end
