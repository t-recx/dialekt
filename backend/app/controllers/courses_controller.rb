class CoursesController < ApplicationController
  before_action :set_course, only: [:show, :update, :destroy]
  before_action :authorize_access_request!, only: [:create, :update, :destroy]

  # GET /courses
  def index
    @courses = Course.all

    render json: @courses, include: { managers: {except: ['password_digest'] } }
  end

  # GET /courses/1
  def show
    render json: @course, include: { managers: {except: ['password_digest'] } }
  end

  # POST /courses
  def create
    @course = Course.new(course_params)

    authorize @course

    if @course.save
      render json: @course, include: { managers: {except: ['password_digest'] } }, status: :created, location: @course
    else
      render json: @course.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /courses/1
  def update
    authorize @course

    if @course.update(course_params)
      render json: @course, include: { managers: {except: ['password_digest'] } }
    else
      render json: @course.errors, status: :unprocessable_entity
    end
  end

  # DELETE /courses/1
  def destroy
    authorize @course

    @course.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_course
      @course = Course.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def course_params
      params.require(:course).permit(:name, :language_id, :description)
    end
end
