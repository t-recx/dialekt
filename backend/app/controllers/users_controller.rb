class UsersController < ApplicationController
  before_action :set_user, only: [:show]

  # GET /users
  def index
    @users = get_users.all

    render json: @users
  end

  # GET /users/1
  def show
    render json: @user
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = get_users.find(params[:id])
    end

    def get_users
      User.select(:id, :username, :email, :admin)
    end
end
