class LoginController < ApplicationController
  before_action :authorize_access_request!, only: [:destroy]

  def create
    user = User.find_by(username: params[:username]) 

    if not user 
      not_found
    else 
      if user.authenticate(params[:password])
        payload  = { user_id: user.id }
        session = JWTSessions::Session.new(payload: payload, refresh_by_access_allowed: true)
        tokens = session.login

        response.set_cookie(JWTSessions.access_cookie,
                            value: tokens[:access],
                            httponly: true,
                            path: '/',
                            secure: Rails.env.production?)

        render json: { csrf: tokens[:csrf], user: user.as_json.except('password_digest') }
      else
        not_authorized
      end
    end
  end

  def destroy
    session = JWTSessions::Session.new(payload: payload)
    session.flush_by_access_payload

    render json: :ok
  end

  private

  def not_found
    render json: { error: 'No user found with the email/password provided' }, status: :not_found
  end
end
