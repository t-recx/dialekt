class ExerciseOptionsController < ApplicationController
  before_action :set_exercise_option, only: [:show, :update, :destroy]

  # GET /exercise_options
  def index
    @exercise_options = ExerciseOption.all

    render json: @exercise_options
  end

  # GET /exercise_options/1
  def show
    render json: @exercise_option
  end

  # POST /exercise_options
  def create
    @exercise_option = ExerciseOption.new(exercise_option_params)

    if @exercise_option.save
      render json: @exercise_option, status: :created, location: @exercise_option
    else
      render json: @exercise_option.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /exercise_options/1
  def update
    if @exercise_option.update(exercise_option_params)
      render json: @exercise_option
    else
      render json: @exercise_option.errors, status: :unprocessable_entity
    end
  end

  # DELETE /exercise_options/1
  def destroy
    @exercise_option.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_exercise_option
      @exercise_option = ExerciseOption.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def exercise_option_params
      params.require(:exercise_option).permit(:option, :correct, :exercise_id)
    end
end
