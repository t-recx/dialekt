class TeachershipsController < ApplicationController
  before_action :set_teachership, only: [:show, :destroy]

  # GET /teacherships
  def index
    @teacherships = Teachership.all

    render json: @teacherships
  end

  # GET /teacherships/1
  def show
    render json: @teachership
  end

  # POST /teacherships
  def create
    @teachership = Teachership.new(teachership_params)

    if @teachership.save
      render json: @teachership, status: :created, location: @teachership
    else
      render json: @teachership.errors, status: :unprocessable_entity
    end
  end

  # DELETE /teacherships/1
  def destroy
    @teachership.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_teachership
      @teachership = Teachership.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def teachership_params
      params.require(:teachership).permit(:user_id, :course_id)
    end
end
